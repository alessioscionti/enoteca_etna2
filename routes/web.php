<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\WineController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PayPalController;
use App\Http\Controllers\WaiterController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\PaypalPaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home')->middleware('auth');

//user
Route::get('/users/all', [UsersController::class, 'index'])->name('users.users');
Route::any('/users/show-admin', [UsersController::class, 'indexadmin'])->name('users.showadmin');
Route::get('/user/detail/{user}', [UsersController::class, 'show'])->name('users.details');
Route::get('/user/edit/{user}',[UsersController::class,'edit'])->name('users.edit');
Route::post('/user/update/{user}',[UsersController::class,'update'])->name('users.update');
Route::get('/user/new',[UsersController::class,'create'])->name('users.new');
Route::post('/user/store',[UsersController::class,'store'])->name('users.store');

//camerieri
Route::any('/waiters/show-waiters', [WaiterController::class, 'index'])->name('waiters.showwaiters');
Route::get('/waiter/detail/{user}', [WaiterController::class, 'show'])->name('waiters.details');
Route::get('/waiter/edit/{user}',[WaiterController::class,'edit'])->name('waiters.edit');
Route::post('/waiter/update/{user}',[WaiterController::class,'update'])->name('waiters.update');
Route::post('/waiter/store',[WaiterController::class,'store'])->name('waiters.store');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');


//vendita vini camerieri
Route::get('/sales', [SaleController::class, 'index'])->name('sales.sales');
Route::get('/sales/create', [SaleController::class, 'create'])->name('sales.create');
Route::post('/sales/store', [SaleController::class, 'store'])->name('sales.store');





//ristoranti
Route::get('/restaurant/all', [RestaurantController::class, 'index'])->name('restaurant.restaurant');
Route::get('/restaurant/create', [RestaurantController::class, 'create'])->name('restaurant.create');
Route::post('/restaurant/store', [RestaurantController::class, 'store'])->name('restaurant.store');

//vini
Route::get('/wines/all', [WineController::class, 'index'])->name('wines.wines');
Route::get('/wine/create', [WineController::class, 'create'])->name('wines.create');
Route::post('/wine/store', [WineController::class, 'store'])->name('wines.store');
Route::get('/wine/detail/{wine}', [WineController::class, 'show'])->name('wines.details');

//ordini
Route::get('/orders/all', [OrderController::class, 'index'])->name('ordini.ordini');
Route::get('/orders/create', [OrderController::class, 'create'])->name('ordini.create');
Route::post('/orders/store', [OrderController::class, 'store'])->name('ordini.store');
Route::post('/orders/pagamento', [OrderController::class, 'pagamento'])->name('pagamento');
Route::post('/orders/cart', [OrderController::class, 'cart'])->name('ordini.cart');
Route::get('/orders/details/{wine}', [OrderController::class, 'winedetails'])->name('ordini.winedetails');
Route::get('/orders/detail/{order}', [OrderController::class, 'show'])->name('ordini.details');
Route::post('/orders/update/{order}', [OrderController::class, 'update'])->name('ordini.update');


//cart
Route::get('/cart', [CartController::class, 'index'])->name('cart.cart');
Route::post('/add-cart', [CartController::class, 'create'])->name('cart.create');
Route::post('/delete', [CartController::class, 'destroy'])->name('cart.delete');
Route::post('/increment', [OrderController::class, 'cartincrementqty'])->name('cart.increment');
Route::post('/decrement', [OrderController::class, 'cartdecrementqty'])->name('cart.decrement');

//statistiche
Route::get('/statistiche', [HomeController::class, 'statistiche'])->name('statistiche.stat');
Route::post('/filterstat', [HomeController::class, 'filterstat'])->name('statistiche.filterstat');
Route::post('/filterorder', [HomeController::class, 'order'])->name('statistiche.filterorder');
Route::post('/statistiche/vini', [HomeController::class, 'vinivenduti'])->name('statistiche.vini');


//PDF
Route::post('generate-pdf/{order}', [PdfController::class, 'generabolla'])->name('pdf.bolla');

//filtri
Route::get('/wines/all/filter', [WineController::class, 'index'])->name('filter.wine');
Route::get('/users/all/filter', [UsersController::class, 'indexadmin'])->name('filter.user');


Route::post('/paypal/order/create',[PaypalPaymentController::class,'create'])->name('paypal.create');
Route::post('/paypal/order/capture',[PaypalPaymentController::class,'capture'])->name('paypal.capture');

Route::get('create-transaction', [PayPalController::class, 'createTransaction'])->name('createTransaction');
Route::post('process-transaction', [PayPalController::class, 'processTransaction'])->name('processTransaction');
Route::get('success-transaction/{idorder}', [PayPalController::class, 'successTransaction'])->name('successTransaction');
Route::get('cancel-transaction', [PayPalController::class, 'cancelTransaction'])->name('cancelTransaction');