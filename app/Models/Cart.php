<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'wine_id',
        'wine_qty',
    ];

    public function wine(){
        return $this->belongsTo(Wine::class,'wine_id');
    }

    public function user(){
        return $this->hasMany(User::class,'user_id');
    }
}
