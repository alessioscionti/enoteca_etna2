<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class warehouse extends Model
{
    use HasFactory;

    protected $fillable=[
        'id_restaurant',
        'id_wine',
        'quantita_ordinata',
        'quantita_restante',
        'quantita_bicchieri',
        'bicchieri_restanti',
        'id_order',
        'attivo',
    ];

    public function wine(){
        return $this->belongsTo(Wine::class,'id_wine');
    }

    public function restaurant(){
        return $this->hasMany(Restaurant::class,'id_restaurant');
    }
    public function order(){
        return $this->hasMany(Order::class,'id');
    }
}
