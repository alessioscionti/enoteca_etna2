<?php

namespace App\Models;

use App\Models\User;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Restaurant extends Model
{
    use HasFactory;

    protected $fillable = [
        'ragione_sociale',
        'indirizzo',
        'citta',
        'provincia',
        'cap',
        'partita_iva',
        'pec',
        'codice_univoco',
        'telefono',
    ];

    public function users(){
        $this->hasMany(User::class,'id_restaurant');
    }

    public function order(){
        return $this->hasMany(Order::class, 'id_restaurant');
    }

    public function waiters(){
        $this->hasMany(waiters::class,'id_restaurant');
    }
}
