<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wine extends Model
{
    use HasFactory;

    protected $fillable=[
        'nome',
        'descrizione',
        'casa_vinicola',
        'prezzo_bottiglia',
        'costo_bottiglia',
        'prezzo_bicchiere',
        'costo_bicchiere',
        'quantita',
        'quantita_bicchieri',
        'id_categoria',
        'immagine'
    ];

    public function warehouse(){
        return $this->belongsTo(warehouse::class,'id_wine');
    }

    public function cart(){
        return $this->belongsTo(Cart::class,'wine_id');
    }

    public function order(){
        return $this->belongsTo(Order::class,'id_wine');
    }

    public function orderprod(){
        return $this->belongsTo(OrderProduct::class,'id_wine');
    }
    
    public function sale(){
        return $this->belongsTo(Sale::class);
    }
    public function categoria(){
        return $this->belongsTo(Categorie::class,'id_categoria');
    }
}
