<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Waiter extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'restaurant_id',
        'nome',
        'cognome',
        'percentuale',
        'punti',
        'provvigioni',
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurant::class,'restaurant_id');
    }

     
    public function user(){
        return $this->belongsTo(User::class,'restaurant_id','id');
    }

    public function sale(){
        return $this->belongsTo(Sale::class);
    }
}
