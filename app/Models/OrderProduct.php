<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;

    protected $fillable=[
        'id_user',
        'id_wine',
        'id_categoria',
        'id_order',
        'id_restaurant',
    ];

    public function wine(){
        return $this->belongsTo(Wine::class,'id_wine');
    }
}
