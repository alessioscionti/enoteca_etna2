<?php

namespace App\Models;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $fillable=[
        'id_user',
        'id_wine',
        'quantita',
        'prezzo',
        'data_ordine',
        'id_restaurant',
        'pagato',
        'evaso',
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurant::class, 'id_restaurant');
    }

    public function wine(){
        return $this->hasMany(Wine::class,'id_wine');
    }
    public function warehouse(){
        return $this->hasMany(warehouse::class,'id');
    }
}
