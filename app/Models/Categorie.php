<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;

    protected $fillable =[
        'nome_categoria',
    ];

    public function wine(){
        return $this->belongsTo(Wine::class);
    }
}
