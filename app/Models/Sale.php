<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable=[
        'warehouse_id',
        'waiter_id',
        'wine_id',
        'restaurant_id',
        'user_id',
        'tavolo',
        'quantita_bottiglie',
        'quantita_bicchieri',
    ];

    public function wine(){
        return $this->belongsTo(Wine::class);
    }

    public function waiter(){
        return $this->belongsTo(Waiter::class);
    }

    public function restaurant(){
        return $this->belongsTo(Restaurant::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
