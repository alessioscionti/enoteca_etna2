<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $order;
    public $orderproduct;

    public function __construct(Order $order, OrderProduct $orderproduct)
    {
        $this->order = $order;
        $this->orderproduct = $orderproduct;
        $id=$order->id;
        $idwine=$orderproduct->id_wine;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order=$this->order;
        $orderproduct=$this->orderproduct;
        $winecount=OrderProduct::where('id_order',$order->id)->selectRaw('count(*) as qty',)->selectRaw('id_wine as id_wine')->groupBy('id_wine')->get();

        return $this->subject('Nuovo Ordine')->view('emails.orders', compact('order','orderproduct','winecount'));
    }
}
