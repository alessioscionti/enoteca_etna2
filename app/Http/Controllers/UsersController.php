<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Waiter;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::paginate(4);
        $waiters=Waiter::paginate(4);
        return view('users.users' ,compact('users','waiters'));
    }

    public function indexadmin(Request $request)
    {
        /* dd($request->ristorante); */
        $seladmin=User::where('admin',1)->orWhere('moderator',1)->paginate(4);
        $seladminrestaurant=User::Where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
        /* $ristorantesearch=$request->ristorante; */
        
        if ($request->role=='admin') {
            if (Auth::user()->admin==1) {
                $role=User::where('admin',1)->paginate(4);
                $rolemoderator=User::where('admin',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            } else {
                $role=User::where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                $rolemoderator=User::where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            }
            
            
        } elseif($request->role=='moderator') {
            if (Auth::user()->admin==1) {
                $role=User::where('moderator',1)->paginate(4);
                $rolemoderator=User::where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            }else{
                $role=User::where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                $rolemoderator=User::where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            }
        }elseif($request->role==null) {

            $role=null;
            $rolemoderator=null;

        }elseif($request->role==1){
            if (Auth::user()->admin==1) {
                $role=User::where('admin',1)->orWhere('moderator',1)->paginate(4);
                $rolemoderator=User::where('admin',1)->orWhere('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            }else{
                $role=User::where('moderator',1)->orWhere('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                $rolemoderator=User::Where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            }
        }
        
        if ($request->ristorante) {
            if (Auth::user()->admin==1) {
                $findrestaurant=Restaurant::where('ragione_sociale', 'like', '%' . $request->ristorante . '%')->get();
                /* dd($findrestaurant); */
                if (!$findrestaurant->isEmpty()) {
                    foreach ($findrestaurant as $key) {
                        $idfound=$key->id;
                    }
                    $restaurant=User::where('id_restaurant',$idfound)->paginate(4);
                    $restaurantmoderator=User::where('id_restaurant',$idfound)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
    
                }else{
                    $idfound=null;
                    $restaurant=null;
                    $restaurantmoderator=null;
                }
                
                
            }else {
                $findrestaurant=Restaurant::where('ragione_sociale', 'like', '%' . $request->ristorante . '%')->get();
                if (!$findrestaurant->isEmpty()) {
                    foreach ($findrestaurant as $key) {
                        $idfound=$key->id;
                    }
                    $restaurant=User::where('id_restaurant',$idfound)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                    $restaurantmoderator=User::where('id_restaurant',$idfound)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                }else{
                    $idfound=null;
                    $restaurant=null;
                    $restaurantmoderator=null;



                }
                
            }
            
            
        } else {
            if (Auth::user()->admin==1) {
                
                $restaurant=null;
                $restaurantmoderator=null;
                
            }else {
                $restaurant=null;
                $restaurantmoderator=null;
            }

        }
        
        if ($request->nome) {
            if (Auth::user()->admin==1) {
                
                $filtername=User::where('name', 'like', '%' . $request->nome . '%')->paginate(4);
                
                $filternamemoderator=User::where('name', 'like', '%' . $request->nome . '%')->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                
            }else {
                $filtername=User::where('name', 'like', '%' . $request->nome . '%')->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                $filternamemoderator=User::where('name', 'like', '%' . $request->nome . '%')->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);

            }
            
            
        } else {
            /* if (Auth::user()->admin==1) {
                
                $filtername=User::where('name', 'like', '%' . $request->nome . '%')->paginate(4);
                $filternamemoderator=User::where('name', 'like', '%' . $request->nome . '%')->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
                
            }else {
                
                $filternamemoderator=User::where('name', 'like', '%' . $request->nome . '%')->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
            } */
            $filtername=null;
            $filternamemoderator=null;

        }
        
        
            
        
        return view('users.showadmin',compact('seladmin','seladminrestaurant','role','rolemoderator','restaurant','restaurantmoderator','filtername','filternamemoderator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restaurants=Restaurant::all();
        $restaurantslogin=Restaurant::where('id',Auth::user()->id_restaurant)->get();
       return view('users.new',compact('restaurants','restaurantslogin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        /* dd($request->admin); */
        if ($request->admin==1) {
            $admin=1;
        }else{
            $admin=0;
        }
        if ($request->moderator==1) {
            $moderator=1;
        }else{
            $moderator=0;
        }
        if ($request->waiters==1) {
            $waiters=1;
        }else{
            $waiters=0;
        }

        $user = new User();
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->admin = $admin;
        $user->moderator = $moderator;
        $user->waiters = $waiters;
        if ($request->admin==1) {
            $user->id_restaurant = 1;
        }
        elseif ($request->restaurant1!=0) {
            $user->id_restaurant = $request->restaurant1;
        }else{
            $user->id_restaurant = $request->restaurant;
        }
        
        $user->save();
        
        $iduser=$user->id;
        if ($request->has('immagine')) {
            $immagine=User::where('id','=',$iduser)->update([
                'immagine'=>$request->file('immagine')->storeAs('public/user/'.$iduser.'', $iduser.'.jpg'),
            ]);
        }
                
        
        if ($user->waiters==1) {
            $waiters=Waiter::where('user_id',$user->id)->count();
        /* dd($waiters); */
        if ($waiters==0) {
            if ($request->restaurant1!=0) {
                $waiter=Waiter::create([
                    'user_id'=>$user->id,
                    'restaurant_id'=>$user->id_restaurant,
                    'nome'=>$request->name,
                    'cognome'=>$request->cognome,
                ]);
                $iduser=$waiter->id;
                if ($request->has('immagine')) {
                    $immagine=Waiter::where('id','=',$iduser)->update([
                        'immagine'=>$request->file('immagine')->storeAs('public/user/waiter/'.$iduser.'', $iduser.'.jpg'),
                    ]);
                }
                
            }else {
                $waiter=Waiter::create([
                    'user_id'=>$user->id,
                    'restaurant_id'=>$user->id_restaurant,
                    'nome'=>$request->name,
                    'cognome'=>$request->cognome,
                ]);
                $iduser=$waiter->id;
                if ($request->has('immagine')) {
                    $immagine=Waiter::where('id','=',$iduser)->update([
                        'immagine'=>$request->file('immagine')->storeAs('public/user/waiter/'.$iduser.'', $iduser.'.jpg'),
                    ]);
                }
                
            }
            
            
        }else {
            $waiters=Waiter::where('user_id',$user->id)->update([
                'user_id'=>$user->id,
                'restaurant_id'=>$user->id_restaurant,
                'nome'=>$request->name,
                'cognome'=>$request->cognome
            ]);
            $iduser=$waiters->id;
            if ($request->has('immagine')) {
                $immagine=Waiter::where('id','=',$iduser)->update([
                    'immagine'=>$request->file('immagine')->storeAs('public/user/waiter/'.$iduser.'', $iduser.'.jpg'),
                ]);
            }
                
        }
        }
        $users=User::paginate(4);
        $seladmin=User::where('admin',1)->orWhere('moderator',1)->paginate(4);
        
        return redirect()->back()->with('message','utente registrato correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $restaurant=Restaurant::where('id',$user->id_restaurant)->get('ragione_sociale');
        foreach ($restaurant as $rest) {
            $ragione_sociale=$rest->ragione_sociale;
            
        }
        return view('users.details', compact('user','ragione_sociale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        
        $waiters=Waiter::where('user_id',$user->id)->get();
        $restaurant=Restaurant::all();
        /* $user=User::find($user->id)->where('id','=',$user->id)->get(); */
        /* dd($user); */
        return view('users.edit',compact('user','restaurant','waiters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user ,Restaurant $restaurant)
    {
        
        $user->name = $request->name;
        if ($request->admin=='on') {
            $request->admin=1;
            $user->admin = $request->admin;
        }else {
            $request->admin=0;
            $user->admin = $request->admin;
        }
        if ($request->moderator=='on') {
            $request->moderator=1;
            $user->moderator = $request->moderator;
        }else {
            $request->moderator=0;
            $user->moderator = $request->moderator;
        }
        if ($request->waiters=='on') {
            $request->waiters=1;
            $user->waiters = $request->waiters;
        }else {
            $request->waiters=0;
            $user->waiters = $request->waiters;
        }
        $user->id_restaurant=$request->id_restaurant;
        
        
        $user->save();
        $iduser=$user->id;
        if ($request->has('immagine')) {
            $immagine=User::where('id','=',$iduser)->update([
                'immagine'=>$request->file('immagine')->storeAs('public/user/'.$iduser.'', $iduser.'.jpg'),
            ]);
        }else{
            $immagine=User::where('id','=',$iduser)->update([
                'immagine'=>null,
            ]);
        }
        if ($user->waiters==1) {
            $waiters=Waiter::where('user_id',$user->id)->count();
        /* dd($waiters); */
        if ($waiters==0) {
            $waiter=Waiter::create([
                'user_id'=>$user->id,
                'restaurant_id'=>Auth::user()->id_restaurant,
                'nome'=>$request->name,
                'cognome'=>$request->cognome,
                'percentuale'=>$request->percentuale,
            ]);
            if ($request->has('immagine')) {
                $immagine=Waiter::where('id','=',$user->id)->update([
                    'immagine'=>$request->file('immagine')->storeAs('public/user/waiter/'.$user->id.'', $user->id.'.jpg'),
                ]);
            }
        }else {
            $waiters=Waiter::where('user_id',$user->id)->update([
                'user_id'=>$user->id,
                'nome'=>$request->name,
                'cognome'=>$request->cognome,
                'percentuale'=>$request->percentuale,
            
            ]);
            if ($request->has('immagine')) {
                $user->immagine=Waiter::where('id','=',$user->id)->update([
                    'immagine'=>$request->file('immagine')->storeAs('public/user/waiter/'.$user->id.'', $user->id.'.jpg'),
                ]);
            }
        }
        }
        
        
        $users=User::all();
        $restaurant=Restaurant::where('id','=',$user->id_restaurant)->get('ragione_sociale');
            foreach ($restaurant as $rest) {
                $ragione_sociale=$rest->ragione_sociale;
                
            }
        $waiters=Waiter::where('user_id',$user->id);
        $seladmin=User::where('admin',1)->orWhere('moderator',1)->paginate(4);
        $seladminrestaurant=User::Where('moderator',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
        return view('users.showadmin', compact('users','ragione_sociale','waiters','seladmin','seladminrestaurant'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
