<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\User;
use App\Models\Wine;
use App\Models\Order;
use App\Models\Waiter;
use App\Models\warehouse;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function home(){
        $users=User::with('waiters')->paginate(5);
        $userrestaurant=User::where('id_restaurant',Auth::user()->id_restaurant)->paginate(5);
        $customer=User::where('id_restaurant',Auth::user()->id_restaurant)->count();
        $customerall=User::all()->count();
        $wines=Wine::all()->count();
        $winesrestaurant=warehouse::where('id_restaurant',Auth::user()->id_restaurant)->count();
        $ordersrestaurant=Order::where('id_restaurant',Auth::user()->id_restaurant)->orderBy('id','DESC')->paginate(5);
        $ordersrestaurantcount=Order::where('id_restaurant',Auth::user()->id_restaurant)->count();
        $orders=Order::all()->count();
        $orderadmin=Order::orderBy('id','DESC')->Paginate(10);
        $sales=Sale::where('user_id',Auth::user()->id)->count();
        $salespaginate=Sale::where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->paginate(5);
        $salespaginaterestaurant=Sale::where('restaurant_id',Auth::user()->id_restaurant)->orderBy('id', 'DESC')->paginate(5);
        $provvigioni=Waiter::where('user_id',Auth::user()->id)->get();
        
        if (!$provvigioni->isEmpty()) {
            foreach ($provvigioni as $key) {
                $guadagno=$key->provvigioni;
            } 
        }else{
            $guadagno=0;
        }
        $spesarist=array();
        $guadagnorist=array();
        $guadagnoristorante=Sale::with('wine')->where('restaurant_id',Auth::user()->id_restaurant)->get();
        foreach ($guadagnoristorante as $key) {
            $guadagnoarray=$key->wine->prezzo_bottiglia*$key->quantita;
            array_push($guadagnorist,$guadagnoarray);
        }
        $totalguadagno=array_sum($guadagnorist);
        $spesaristorante=Order::Where('id_restaurant',Auth::user()->id_restaurant)->get();
        
        foreach ($spesaristorante as $key) {
            
            $spesaarray=$key->prezzo;
            array_push($spesarist,$spesaarray);
        }
        
        $totalespesa=array_sum($spesarist);

        $guadagnoadmin=array();
        $guadagnoetna=Order::where('pagato',1)->get();
        foreach ($guadagnoetna as $key) {
            $guadagnoetnaarray=$key->prezzo;
            array_push($guadagnoadmin,$guadagnoetnaarray);
        }
        $totalguadagnoetna=array_sum($guadagnoadmin);
        
        
        return view('home', compact(
            'guadagno',
            'users',
            'customer',
            'wines',
            'orders',
            'orderadmin',
            'userrestaurant',
            'ordersrestaurantcount',
            'customerall',
            'ordersrestaurant',
            'winesrestaurant',
            'sales',
            'salespaginate',
            'totalguadagno',
            'salespaginaterestaurant',
            'totalespesa',
            'totalguadagnoetna'
        ));
    }
    public function statistiche(){
        $restaurants=Restaurant::all();
        $wine=Wine::all();
        $user=User::all();
        $warehouse=warehouse::all();

        return view('statistiche.stat',compact('restaurants','wine','user','warehouse'));
    }

    public function filterstat(){

        $idrestaurant=Restaurant::where('id',$_POST['restaurant_id'])->get();
        $orderselected=Order::where('id_restaurant',$_POST['restaurant_id'])->get();
        return response()->json(['id'=>$idrestaurant]);
    }

    public function order(Request $request){

        $date=$_POST['date'];
        $datesplit=explode('-',$date);
        $datastart=Carbon::parse($datesplit[0])->format('Y-m-d');
        $dataend=Carbon::parse($datesplit[1])->format('Y-m-d');
        $orderselected=Order::where('id_restaurant',$_POST['restaurant_id'])->wherebetween('data_ordine',[$datastart,$dataend])->get();

        if ($orderselected->isEmpty()) {
            $orderselect=0;
        } else {
            $orderselect=Order::where('id_restaurant',$_POST['restaurant_id'])->wherebetween('data_ordine',[$datastart,$dataend])->get();
        }
        
        return response()->json(['id'=>$orderselect]);
    }

    public function vinivenduti(Request $request)
    {
        /* $idorder=array(); */
        $order=array();
        $wines=array();
        $whouse=array();
        
        /* $warehouse=array(); */
        $qtatot_ordinata=array();
        $qtatot_restante=array();
       
        foreach ($request->all() as $keys) {
            $data=$request->data;
            $idorder=$request->idorder;
            $countorder=count($idorder);
            /* dd($countorder); */
            $qty=$request->qty;
            foreach ($idorder as $key => $value) {
                
                $orderProduct[$key]=warehouse::with('wine')->Where('id_order',$key)->get();
                /* dd($orderProduct[$key]); */
                foreach ($orderProduct[$key] as $wine) {
                    /* dd($wine); */
                    if ($wine->id_order==$key) {
                        $vinivenduti[$key][$wine->id_wine]=$wine->quantita_ordinata-$wine->quantita_restante;
                        $vinirestanti[$key][$wine->id_wine]=$wine->quantita_restante;
                    }
                $orderProduct[$key]=warehouse::with('wine','order')->Where('id_order',$key)->selectRaw('sum(quantita_restante) as quantita_restante')->selectRaw('sum(quantita_ordinata) as quantita_ordinata')->selectRaw('id_wine as id_wine')->groupBy('id_wine')->orderBy('id_wine')->get();

                }
            }
            /* dd($orderProduct); */
            /* dd($orderProduct); */
            $wines=Wine::all();
        }
        return view('statistiche.vini',compact('vinivenduti','vinirestanti','wines','orderProduct','key'));
    }
}
