<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Srmklive\PayPal\Services\PayPal as PayPalClient;

class PaypalPaymentController extends Controller
{
    public function create(Request $request)
    {

        $cart=Cart::where('id',$request->id_cart)->get();
        

        $provider = new PayPalClient;
        $provider = \PayPal::setProvider();



        $provider->setCurrency('EUR');
        $provider->getAccessToken();

        $data = json_decode('{
            "intent": "CAPTURE",
            "purchase_units": [
              {
                "amount": {
                  "currency_code": "EUR",
                  "value": "100.00"
                }
              }
            ]
        }', true);
        
        $order = $provider->createOrder($data);
        

        dd($order);
    
        /* $data = json_decode($request->getContent(), true);
        $this->paypalClient->setApiCredentials(config('paypal'));
        $token = $this->paypalClient->getAccessToken();
        
        $this->paypalClient->setAccessToken($token);
        $order = $this->paypalClient->createOrder([
            "intent"=> "CAPTURE",
            "purchase_units"=> [
                 [
                    "amount"=> [
                        "currency_code"=> "EUR",
                        "value"=> $data['amount']
                    ],
                     'description' => 'test'
                ]
            ],
        ]);
        $mergeData = array_merge($data,['status' => TransactionStatus::PENDING, 'vendor_order_id' => $order['id']]);
        DB::beginTransaction();
        Order::create($mergeData);
        DB::commit();
        return response()->json($order);
 */

        //return redirect($order['links'][1]['href'])->send();
       // echo('Create working');
    }

    public function capature($order,$provider){


        $order_id = $order['id'];
        $order = $provider->capturePaymentOrder($order_id);
        dd($order);
    }
}
