<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Waiter;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WaiterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $waiters=User::where('waiters',1)->paginate(4);
        
        $waitersrestaurant=User::where('waiters',1)->where('id_restaurant',Auth::user()->id_restaurant)->paginate(4);
        
        /* dd($selwaitersrestaurant); */
        
        return view('waiters.showwaiters',compact('waiters','waitersrestaurant','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('waiters.details',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user ,Waiter $waiter)
    {
        $waiter=Waiter::where('user_id',$user->id)->get();
        if (!$waiter->isEmpty()) {
            foreach ($waiter as $key ) {
                if ($key->cognome==NULL) {
                    $cognome='non definito';
                }else{
    
                    $cognome=$key->cognome;
                }
            }  
        }else {
            $cognome="NON DEFINITO";
        }
        
        $restaurant=Restaurant::all();
        return view('waiters.edit',compact('user','cognome','restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        
        if ($request->admin=='on') {
            $userupdate=User::where('id',$request->user_id)->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'admin'=>1,
                'moderator'=>0,
                'waiters'=>0,
                'id_restaurant'=>$request->id_restaurant,
            ]);
            $updatewaiter=Waiter::where('user_id',$request->user_id)->delete();
        }elseif($request->moderator=='on'){
            $userupdate=User::where('id',$request->user_id)->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'moderator'=>1,
                'admin'=>0,
                'waiters'=>0,
                'id_restaurant'=>$request->id_restaurant,

            ]);
            $updatewaiter=Waiter::where('user_id',$request->user_id)->delete();
        }else{
            
            $userupdate=User::where('id',$request->user_id)->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'id_restaurant'=>$request->id_restaurant,

            ]);

            $findwaiter=Waiter::where('user_id',$request->user_id)->get();
            if ($findwaiter->isEmpty()) {
                $createwaiter=Waiter::create([
                    'user_id'=>$request->user_id,
                    'restaurant_id'=>$request->id_restaurant,
                    'nome'=>$request->name,
                    'cognome'=>$request->cognome,
                    'percentuale'=>$request->percentuale,
                ]);
            }
            $updatewaiter=Waiter::where('user_id',$request->user_id)->update([
                'nome'=>$request->name,
                'cognome'=>$request->cognome,
                'restaurant_id'=>$request->id_restaurant,
                
            ]);
        }
        

        return redirect()->back()->with('message', "utente aggiornato");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Waiter $waiter)
    {
        //
    }
}
