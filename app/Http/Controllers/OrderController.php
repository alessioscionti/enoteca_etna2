<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Cart;
use App\Models\Wine;
use App\Models\Order;
use App\Models\warehouse;
use App\Models\Restaurant;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders=Order::orderBy('id','DESC')->paginate(6);
        $ordersrestaurant=Order::where('id_restaurant',Auth::user()->id_restaurant)->orderBy('id','DESC')->paginate(10);
        $wine=Wine::all();
        $restaurant=Restaurant::all();
        return view('ordini.ordini', compact('orders','wine','restaurant','ordersrestaurant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wines=Wine::all();
        
        return view('ordini.create', compact('wines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        //dichiaro i 2 array per i vini e le quantita che mi arriveranno dalla request
        $wines=array();
        $quantity=array();
        $orderprice=array();

        //ciclo la request per trovare i vini e le quantita richieste
        foreach ($request->all() as $key => $value) {
            $wines=$request->wine;
            
            foreach ($wines as $key) {
                //cerco nel database i vini corrispondenti ai vini richiesti dal cliente e ne estraggo i dettagli(assegno all'array chiave=>valore, $key in modo da associare la chiave all'id del vino)
                $wines[$key]=Wine::where('id',$key)->get();
                
                /* dd($wines); */
                //ciclo i vini trovati per poter estrarre il prezzo singolo
                foreach ($wines[$key] as $key) {
                    $prezzo=$key->prezzo_bottiglia;
                };
                foreach ($request->quantity as $keys =>$value) {
                    if ($key->id==$keys) {
                        $quantity[$key->id]=$value;
                    }
                    
                    if ($key->id==$keys) {
                        $orderprice[$key->id]=$prezzo*($value);
                    }
                }
            }
            /* dd($orderprice,$wines,$quantity); */
            $total= array_sum($orderprice);
        }
        
        /* dd($orderprice,$orderwine); */
        if ($request->restaurant!=0) {
            $order=Order::create([
                'id_user'=>Auth::user()->id,
                'quantita'=>array_sum($quantity),
                'prezzo'=>$total,
                'data_ordine'=>Carbon::now(),
                'id_restaurant'=>$request->restaurant,
                'pagato'=>0,
                'evaso'=>0,
            ]);
        }else{

        
        $order=Order::create([
            'id_user'=>Auth::user()->id,
            'quantita'=>array_sum($quantity),
            'prezzo'=>$total,
            'data_ordine'=>Carbon::now(),
            'id_restaurant'=>Auth::user()->id_restaurant,
            'pagato'=>0,
            'evaso'=>0,
        ]);
        }
        //da sistemare, se vino gia esistente, aumentare la quantità e non registrare nuova row
        $whouse=Cart::where('user_id',Auth::user()->id)->get();
        
        /* if ($request->restaurant==0) { */
            foreach ($whouse as $cart) {
                $bicchierivini=Wine::where('id',$cart->wine_id)->get();
                foreach ($bicchierivini as $key) {
                    $quantita_bicchieri=$key->quantita_bicchieri;
                    $bicchieri_restanti=$key->bicchieri_restanti;
                }
                for ($i=0; $i < $cart->wine_qty; $i++) {
                    if ($request->restaurant==null) {
                        $warehouse=warehouse::create([
                            'id_restaurant'=>Auth::user()->id_restaurant,
                            'id_wine'=>$cart->wine_id,
                            'quantita_ordinata'=>1,
                            'quantita_restante'=>1,
                            'quantita_bicchieri'=>$quantita_bicchieri,
                            'bicchieri_restanti'=>$quantita_bicchieri,
                            'id_order'=>$order->id,
                        ]);
                    }else{
                        $warehouse=warehouse::create([
                            'id_restaurant'=>$request->restaurant,
                            'id_wine'=>$cart->wine_id,
                            'quantita_ordinata'=>1,
                            'quantita_restante'=>1,
                            'quantita_bicchieri'=>$quantita_bicchieri,
                            'bicchieri_restanti'=>$quantita_bicchieri,
                            'id_order'=>$order->id,
                        ]);
                    }
                    
                }
                               
            }

        
        foreach ($quantity as $k => $v) {
            $index=1;
            
            if ($index=$k) {
                for ($i=1; $i <= $v ; $i++) { 
                    $orderproduct=OrderProduct::create([
                        'id_user'=> Auth::user()->id,
                        'id_order'=>$order->id,
                        'id_wine'=>$index,
                        'id_restaurant'=>Auth::user()->id_restaurant,
                    ]);

                    $wine=Wine::where('id',$index)->get('quantita');
                    foreach ($wine as $w) {
                        $qty=$w->quantita;
                    }
                    $qty--;
                    $wine=Wine::where('id',$index)->update(['quantita'=>$qty]);
                }
                $index++;
            }
        }
        /* Mail::to('alessioscionti@gmail.com')->send(new OrderMail($order,$orderproduct)); */
        //completato l'ordine posso svuotare il carrello e tutte le entità presenti nella tabella dell'utente che ha appena effettuato l'ordine
        $cart=Cart::where('user_id',Auth::user()->id)->delete();

        return redirect()->back()->with('message', "ordine N° $order->id inserito, dal <a href='/orders/all'>Riepilogo ordini</a> puoi completare il pagamento");
        
    }
    public function cartincrementqty(Request $request){
        $cartupdate=Cart::where('user_id',Auth::user()->id)->where('wine_id',$_POST['id_wine'])->increment('wine_qty',$_POST['inc_1']);

        return response()->json("quantita aggiornata");

    }
    public function cartdecrementqty(Request $request){

        $cartupdate=Cart::where('user_id',Auth::user()->id)->where('wine_id',$_POST['id_wine'])->decrement('wine_qty',$_POST['dec_1']);

        return response()->json("quantita aggiornata");
    }
    public function cart(Request $request){
                

        //dichiaro i 2 array per i vini e le quantita che mi arriveranno dalla request
        $wines=array();
        $quantity=array();
        $orderprice=array();

        //ciclo la request per trovare i vini e le quantita richieste
        foreach ($request->all() as $key => $value) {
            $wines=$request->wine;
            
            foreach ($wines as $key) {
                //cerco nel database i vini corrispondenti ai vini richiesti dal cliente e ne estraggo i dettagli(assegno all'array chiave=>valore, $key in modo da associare la chiave all'id del vino)
                $wines[$key]=Wine::where('id',$key)->get();
                
                /* dd($wines); */
                //ciclo i vini trovati per poter estrarre il prezzo singolo
                foreach ($wines[$key] as $key) {
                    $prezzo=$key->prezzo_bottiglia;
                };
                foreach ($request->quantity as $keys =>$value) {
                    if ($key->id==$keys) {
                        $quantity[$key->id]=$value;
                    }
                    
                    if ($key->id==$keys) {
                        $orderprice[$key->id]=$prezzo*($value);
                    }
                }
            }
            /* dd($orderprice,$wines,$quantity); */
            $total= array_sum($orderprice);
        }

        $restaurants=Restaurant::all();
        return view('ordini.cart',compact('restaurants'));
    }

    public function winedetails(Wine $wine){

        
        return view('ordini.winedetails',compact('wine'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $orderproduct=OrderProduct::where('id_order',$order->id)->get();
        /* dd($orderproduct); */
        foreach ($orderproduct as $key) {
            
            /* $winecount= DB::table('order_products')
            ->select('id_wine', DB::raw('count(*) as qty'))
            ->groupBy('id_wine')
            ->get(); */

            $winecount=OrderProduct::where('id_order',$key->id_order)->selectRaw('count(*) as qty',)->selectRaw('id_wine as id_wine')->groupBy('id_wine')->get();
            /* dd($winecount); */
            
        }
        
        /* dd($winecount); */
        return view('ordini.details', compact('order','orderproduct','winecount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        
        if ($request->pagato=='si') {
            $updatepayment=Order::where('id',$order->id)->update(['pagato'=> '1']);
            $updatewarehouse=warehouse::where('id_order',$order->id)->update(['attivo'=>'1']);
        }elseif ($request->pagato=='no') {
            $updatepayment=Order::where('id',$order->id)->update(['pagato'=> '2']);
            $updatewarehouse=warehouse::where('id_order',$order->id)->update(['attivo'=>'0']);

        }
        if ($request->evaso=='si') {
            $updatepayment=Order::where('id',$order->id)->update(['evaso'=> '1']);
        }elseif ($request->evaso=='no') {
            $updatepayment=Order::where('id',$order->id)->update(['evaso'=> '2']);
        }
        return redirect()->back()->with('message','ordine aggiornato');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function pagamento($order){

        dd($order);
        return view('pagamento',compact('order'));
    }
    
}
