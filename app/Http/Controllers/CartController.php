<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Wine;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart=Cart::where('user_id',Auth::user()->id)->get();
        /* dd($cart); */

        $wine=Wine::all();
        
        $restaurants=Restaurant::all();
        return view('cart.cart',compact('cart','wine','restaurants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cart=Cart::all();
        if ($cart->isEmpty()) {
            $wine_id=$_POST['product_id'];
            $quantity_id=$_POST['quantity'];
            $user_id=$_POST['user_id'];

        $wine=Wine::where('id',$wine_id)->get();
        foreach ($wine as $key) {
            $cart=Cart::create([
                'user_id'=>$user_id,
                'wine_id'=>$key->id,
                'wine_qty'=>$quantity_id,
            ]);
        }
        return response()->json("prodotto inserito nel carrello");
        }else {
            
            $wine_id=$_POST['product_id'];
            $quantity_id=$_POST['quantity'];
            $user_id=$_POST['user_id'];
            $wine=Wine::where('id',$wine_id)->get();
            $cart=Cart::where('user_id',Auth::user()->id)->where('wine_id',$wine_id)->get();
            if ($cart->isEmpty()) {
                foreach ($wine as $key) {
                    $cart=Cart::create([
                        'user_id'=>$user_id,
                        'wine_id'=>$key->id,
                        'wine_qty'=>$quantity_id,
                    ]);
                }
            }else {
            $cart=Cart::where('user_id',Auth::user()->id)->where('wine_id',$wine_id)->increment('wine_qty',$quantity_id);
                
            }

            return response()->json("prodotto inserito nel carrello");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        
        $deleteitem=Cart::where('id',$_POST['delete_item'])->delete();
        return response()->json("Prodotto Rimosso");
    }
}
