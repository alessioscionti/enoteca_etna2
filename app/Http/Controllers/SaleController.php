<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\Wine;
use App\Models\Waiter;
use App\Models\warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $waiter=Waiter::where('user_id',Auth::user()->id)->get();
        $sales=Sale::where('user_id',Auth::user()->id)
        ->selectRaw('DATE(created_at) as date')
        ->selectRaw('sum(quantita_bottiglie) as qty_bottiglie')
        ->selectRaw('sum(quantita_bicchieri) as qty_bicchieri')
        ->groupBy('date')
        ->paginate(6);
        /* dd($sales); */
        
        return view('sales.sales',compact('waiter','sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wines=warehouse::where('id_restaurant',Auth::user()->id_restaurant)->where('attivo',1)->selectRaw('sum(quantita_restante) as qty')->selectRaw('sum(quantita_bicchieri) as qty_bicchieri')->selectRaw('id_wine as id_wine')->groupBy('id_wine')->get();
        /* dd($wines); */
        $waiter=Waiter::where('user_id',Auth::user()->id)->get();
        foreach ($waiter as $key) {
            $waiter_id=$key->id;
        }
        return view('sales.create',compact('wines','waiter_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request); */
        if ($request->tipo=="bottiglia") {
            $wine=$request->wine_id;
            $split=explode('-',$wine);
            $wine_id=$split[0];
            /* dd($request); */
            if ($request->quantita==0) {
                return redirect()->back()->with('danger', 'ordine non inserito, devi selezionare un vino');
            }else{
            
    
            $selectwine=warehouse::where('id_restaurant',Auth::user()->id_restaurant)->where('id_wine',$wine_id)->whereRaw('quantita_bicchieri = bicchieri_restanti')->whereRaw('quantita_restante > 0')->orderBy('id_order')->get();
            /* dd($selectwine); */
            $qta=$request->quantita;
            foreach ($selectwine as $key => $value) {
                /* dd($value['id']); */
                if ($value->quantita_restante <= $qta) {
                    $qtarest=$value->quantita_restante;
                    
                    $qta=$qta-$value->quantita_restante;
                    $value->decrement('quantita_restante',$qtarest);
                    /* $value->update(['quantita_bicchieri'=>0]); */
                    $value->update(['bicchieri_restanti'=>0]);
                    $sale=Sale::create([
                        'warehouse_id'=>$value['id'],
                        'waiter_id'=>$request->waiter_id,
                        'wine_id'=>$wine_id,
                        'restaurant_id'=>$request->restaurant_id,
                        'user_id'=>$request->user_id,
                        'tavolo'=>$request->tavolo,
                        'quantita_bottiglie'=>$qtarest,
                        'quantita_bicchieri'=>0,
                    ]);
                }elseif($value->quantita_restante > $qta){
                    /* $qta=$qta-$value->quantita_restante; */
    
                    $value->decrement('quantita_restante',$qta);
                    if($qta=0){
                        break;
                    }
                }
            }
    
            }
        } elseif($request->tipo=="bicchiere") {
            $wine=$request->wine_id;
            $bicchierivino=Wine::Where('id',$request->wine_id)->get();
            foreach ($bicchierivino as $key) {
                $bicchieri=$key->quantita_bicchieri;
            }
            $split=explode('-',$wine);
            $wine_id=$split[0];
            /* dd($request); */
            if ($request->quantita==0) {
                return redirect()->back()->with('danger', 'ordine non inserito, devi selezionare un vino');
            }else{
                /* if ($request->quantita>=$bicchieri) {
                    $sale=Sale::create([
                        'waiter_id'=>$request->waiter_id,
                        'wine_id'=>$wine_id,
                        'restaurant_id'=>$request->restaurant_id,
                        'user_id'=>$request->user_id,
                        'tavolo'=>$request->tavolo,
                        'quantita_bottiglie'=>1,
                        'quantita_bicchieri'=>$request->quantita,
                    ]);
                }else{
                    $sale=Sale::create([
                        'waiter_id'=>$request->waiter_id,
                        'wine_id'=>$wine_id,
                        'restaurant_id'=>$request->restaurant_id,
                        'user_id'=>$request->user_id,
                        'tavolo'=>$request->tavolo,
                        'quantita_bottiglie'=>0,
                        'quantita_bicchieri'=>$request->quantita,
                    ]);
                } */
            
    
            $selectwine=warehouse::where('id_restaurant',Auth::user()->id_restaurant)->where('id_wine',$wine_id)->whereRaw('quantita_ordinata = quantita_restante')->whereRaw('bicchieri_restanti > 0')->orderBy('id_order')->get();
            /* dd($selectwine); */
            $qta=$request->quantita;
            foreach ($selectwine as $key => $value) {
                /* dd($value['id']); */
                /* dd($value->bicchieri_restanti <= $qta); */
                if ($value->bicchieri_restanti <= $qta) {
                    $qtarest=$value->bicchieri_restanti;
                    
                    $qta=$qta-$value->bicchieri_restanti;
                    $value->decrement('bicchieri_restanti',$qtarest);
                    /* $value->update(['quantita_ordinata'=>0]); */
                    $value->update(['quantita_restante'=>0]);
                    $sale=Sale::create([
                        'warehouse_id'=>$value['id'],
                        'waiter_id'=>$request->waiter_id,
                        'wine_id'=>$wine_id,
                        'restaurant_id'=>$request->restaurant_id,
                        'user_id'=>$request->user_id,
                        'tavolo'=>$request->tavolo,
                        'quantita_bottiglie'=>1,
                        'quantita_bicchieri'=>$qtarest,
                    ]);
                }elseif($value->bicchieri_restanti > $qta){
                    /* $qta=$qta-$value->quantita_restante; */
                    $sale=Sale::create([
                        'warehouse_id'=>$value['id'],
                        'waiter_id'=>$request->waiter_id,
                        'wine_id'=>$wine_id,
                        'restaurant_id'=>$request->restaurant_id,
                        'user_id'=>$request->user_id,
                        'tavolo'=>$request->tavolo,
                        'quantita_bottiglie'=>0,
                        'quantita_bicchieri'=>$qta,
                    ]);
                    $value->decrement('bicchieri_restanti',$qta);

                    if($qta=1){
                        break;
                    }
                }
            }
    
            }
        }
        
        $findpercent=Waiter::where('id',$request->waiter_id)->get();
        foreach ($findpercent as $key) {
            
            $percent=$key->percentuale;
        }
        $pointwaiter=Waiter::where('id',$request->waiter_id)->increment('punti',$request->quantita);
        $point=Waiter::where('id',$request->waiter_id)->get('punti');

        $provvigioni=Sale::where('waiter_id',$request->waiter_id)->where('quantita_bottiglie','>',0)->get();
        /* dd($provvigioni); */
        if (!$provvigioni->isEmpty()) {
            foreach ($provvigioni as $key) {
                $bottiglie=Wine::where('id',$key->wine_id)->get();
                foreach ($bottiglie as $key) {
                    $prezzobottiglia=$key->prezzo_bottiglia*$request->quantita;
                    
                }
                $guadagno=(($prezzobottiglia*$percent)/100);
                
            }
            $updateguadagno=Waiter::where('id',$request->waiter_id)->increment('provvigioni',$guadagno);
    
        }else {
            $updateguadagno=Waiter::where('id',$request->waiter_id)->increment('provvigioni',0);

        }
        return redirect()->back()->with('success','Vendita inserita');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
