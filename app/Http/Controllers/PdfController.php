<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Wine;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function generabolla(Request $request, Order $order) {
        /* dd($request); */
        $order = array(
            'idorder' => $request->idorder,
            'ragione_sociale'=>$request->ragione_sociale,
            'data_ordine'=>$request->data_ordine,
            'costo_ordine'=>$request->costo_ordine,
        );
        $winecount=OrderProduct::where('id_order',$request->idorder)->selectRaw('count(*) as qty',)->selectRaw('id_wine as id_wine')->groupBy('id_wine')->get();
        
        $pdf = PDF::loadView('pdf.bolla', ['order' => $order],['winecount'=>$winecount]);
        return $pdf->download('test.pdf');
    }
}
