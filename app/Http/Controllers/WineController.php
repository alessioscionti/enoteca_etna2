<?php

namespace App\Http\Controllers;

use App\Models\Wine;
use App\Http\Controllers\Controller;
use App\Models\Categorie;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class WineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* dd($request->nome_vino); */
        $wines=Wine::all();
        
        $filtername=Wine::where('nome', 'like', '%' . $request->nome . '%')->get();
        /* dd($filtername); */
        $filterbrand=Wine::where('casa_vinicola',$request->brand)->get();

        $filtercat=Wine::where('id_categoria',$request->categorie)->get();

        $filterprice=Wine::whereBetween('prezzo_bottiglia',[$request->min,$request->max])->get();
        
        
        
            
        

        /* dd($filtername); */
        
        
        return view('wines.wines',compact('wines','filterbrand','filtercat','filtername','filterprice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=Categorie::all();
        return view('wines.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request); */
        $wines=Wine::create([
            'nome'=>$request->nome,
            'descrizione'=>$request->descrizione,
            'casa_vinicola'=>$request->brand,
            'prezzo_bottiglia'=>$request->prezzo_bottiglia,
            'costo_bottiglia'=>$request->costo_bottiglia,
            'prezzo_bicchiere'=>$request->prezzo_bicchiere,
            'costo_bicchiere'=>$request->costo_bicchiere,
            'quantita'=>$request->quantita,
            'quantita_bicchieri'=>$request->quantita_bicchieri,
            'id_categoria'=>$request->categoria,
        ]);
        /* $wine=Wine::find($request->id_wine)->get(); */
        $idwine=$wines->id;
        /* dd($idwine); */
        $immagine=Wine::where('id','=',$idwine)->update([
            'immagine'=>$request->file('immagine')->storeAs('public/vini/'.$idwine.'', $idwine.'.jpg'),
        ]);

        return redirect()->back()->with('message', 'vino inserito correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wine  $wine
     * @return \Illuminate\Http\Response
     */
    public function show(Wine $wine)
    {
        return view('wines.details',compact('wine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wine  $wine
     * @return \Illuminate\Http\Response
     */
    public function edit(Wine $wine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wine  $wine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wine $wine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wine  $wine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wine $wine)
    {
        //
    }
}
