<style>
    thead,
tbody,
tfoot,
tr,
td,
th {
    width: auto!important;
}
</style>
<x-layout>
    <main class="main-content">
        <div class="container-xxl">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h2>STATISTICHE</h2>
                    <h3>Seleziona Ristorante:</h3>
                    <select class="restaurant" name="restaurant" id="">
                        <option value=""selected disabled>--</option>
                        @foreach ($restaurants as $restaurant)
                        <option class="restaurant_id" value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="container-xxl date mt-5" hidden>
                    <div class="row justify-content-center">
                        <div class="col text-center">
                            <h3 for="order">Seleziona Date</h3><br>
                            <div class="d-flex justify-content-center">
                                <input type="text" name="daterange" value="" class="datarange form-control" style="width: 200px;"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-xxl order mt-2" hidden>
                    <div class="row justify-content-center">
                        <div class="col text-center">
                            <button type="submit" class="vieworder btn btn-dark mt-3">Visualizza Ordini effettuati</button>
                            
                        </div>
                    </div>
                </div>
                <div class="container-xxl mt-5 show-rist" style="width:auto!important;" hidden>
                    <h2 class="text-center">Ristorante</h2>
                    <div class="cardstat">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="col mt-1">
                                    <thead>
                                    <tr>
                                        <th scope="col-1" class="col-1 id">ID</th>
                                        <th scope="col-7" class="col-7">Ragione Sociale</th>
                                        <th scope="col-2" class="col-2">Partita iva</th>
                                        <th scope="col-2" class="col-2">Citta</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><input class="id" readonly value="" type="text" style="border:none;font-weight:400;"></th>
                                        <td><input class="ragione_sociale input-transition" readonly value="" type="text" style="border:none;font-weight:400;"></td>
                                        <td><input class="partita_iva" readonly value="" type="text" style="border:none;font-weight:400;"></td>
                                        <td><input class="citta" value="" readonly type="text" style="border:none;font-weight:400;"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-xxl show-order mt-2" style="width:auto!important;" hidden>
                    <h2 class="text-center">Ordini Effettuati</h2>
                    <div class="cardstat">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="noresult">

                                </div>
                                <form action="{{route('statistiche.vini')}}" method="POST">
                                    @csrf
                                <table class="mt-1 results">
                                    <thead>
                                    <tr>
                                        <th scope="col" class="col-1 id">ID</th>
                                        <th scope="col" class="col-3">quantita bottiglie</th>
                                        <th scope="col" class="col-1">prezzo</th>
                                        <th scope="col" class="col-4">Data ordine</th>
                                        <th scope="col" class="col-1">Pagato</th>
                                        <th scope="col" class="col-2">Azioni</th>
                                    </tr>
                                    </thead>
                                        <tbody class="result">
                                            {{-- inject response Ajax --}}
                                        </tbody>
                                        <div class="row justify-content-center">
                                            <div class="col-12 text-center">
                                                <button type="submit" class="btn btn-dark mb-1">Visualizza statistiche vendita</button>
                                            </div>
                                        </div>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>


$(function() {

  $('input[name="daterange"]').daterangepicker({
    "opens": 'center',
    "showDropdowns":false,
    "drops":'auto',
    "singleDatePicker":false,
    "linkedCalendars":false,
    
  }, function(start, end) {
      console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
    
    $('.datarange').on('apply.daterangepicker', function(ev, picker) {
        $(".show-order").load(location.href + " .show-order");
    });
});

$('.restaurant').change(function (e) { 
    e.preventDefault();
    $('.show-rist').attr('hidden',false);
    $('.date').attr('hidden',false);
    $('.order').attr('hidden',false);
    $(".show-order").load(location.href + " .show-order");
    $('.datarange').attr('hidden',false);
    let restaurant_id=$(this).val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        method: "POST",
        url: "/filterstat",
        data: {
                'restaurant_id':restaurant_id,
            },
        success: function (response) {
            let res = JSON.stringify(response);
            let respons = JSON.parse(res);
                
            $.each(respons,function(key,value){
                $.each(value,function(keys,values){
                    let ragione_sociale=$('.ragione_sociale').val(values['ragione_sociale']);
                    let partita_iva=$('.partita_iva').val(values['partita_iva']);
                    let id=$('.id').val(values['id']);
                    let citta=$('.citta').val(values['citta']);
                })
            });        
        }
    });
});

$('.vieworder').click(function (e) { 
    e.preventDefault();
    
    let date=$('.datarange').val();
    
    let restaurant_id=$('.restaurant').val();
        
        $('.show-order').attr('hidden',false);
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $.ajax({
        method: "POST",
        url: "/filterorder",
        data: {
                'date':date,
                'restaurant_id':restaurant_id,
              },
        success: function (response) {
            if (response.id==0) {
                var $tr = $('.noresult').append('<h2>NESSUNA VENDITA TROVATA</h2>');
                $('.results').attr('hidden',true);
            }else{
                let res = JSON.stringify(response);
                let respons = JSON.parse(res);
                $.each(respons,function(key,value){
                    $.each(value,function(i,item){
                            /* console.log(item.id); */
                        if (item.pagato==2||item.pagato==0) {
                            var $tr = $('.result').append('<tr><th scope="row"><input class="idorder" name="idorder['+item.id+']" readonly value="'+item.id+'" type="text" style="border:none;font-weight:400;"></th><td><input name="qty['+item.id+']" readonly value="'+item.quantita+'" type="text" style="border:none;font-weight:400;"></td><td><input name="prezzo['+item.id+']" readonly value="'+item.prezzo+'" type="text" style="border:none;font-weight:400;"></td><td><input name="data['+item.id+']" readonly value="'+item.data_ordine+'" type="text" style="border:none;font-weight:400;"></td><td><input readonly value="NO" type="text" style="border:none;font-weight:600;color:red"></td><td><a class="btn btn-dark" href="/orders/detail/'+item.id+'" target="_blank">Dettagli</a></td></tr>');
                        } else {
                            var $tr = $('.result').append('<tr><th scope="row"><input class="idorder" name="idorder['+item.id+']" readonly value="'+item.id+'" type="text" style="border:none;font-weight:400;"></th><td><input name="qty['+item.id+']" readonly value="'+item.quantita+'" type="text" style="border:none;font-weight:400;"></td><td><input name="prezzo['+item.id+']" readonly value="'+item.prezzo+'" type="text" style="border:none;font-weight:400;"></td><td><input name="data['+item.id+']" readonly value="'+item.data_ordine+'" type="text" style="border:none;font-weight:400;"></td><td><input readonly value="SI" type="text" style="border:none;font-weight:600;color:green"></td><td><a class="btn btn-dark" href="/orders/detail/'+item.id+'" target="_blank">Dettagli</a></td></tr>');
                        }
                        $('.vieworder').attr('hidden',false);
                    });
                });
            }
        }
    });
});

    </script>
</x-layout>