<x-layout>
    <main class="main-content">
        <div class="container-xxl">
            @foreach ($orderProduct as $key=> $order)
            
                <div class="container m-1 cardstat">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="row justify-content-center">
                                <?php 
                                    foreach($order as $item){
                                        
                                        $idorder=$key;
                                    }
                                ?>
                                <div class="col-12 text-center">
                                    <h1>Ordine N° {{$key}}</h1>
                                </div><hr>
                            @foreach ($order as $item)
                            <div class="col text-center">
                                <?php $percent=(($item->quantita_ordinata-$item->quantita_restante)/$item->quantita_ordinata)*100;
                                      $venduto=$item->quantita_ordinata-$item->quantita_restante;
                                ?>
                                <h2>{{$item->wine->nome}}</h2>
                                <p>ordinato: {{$item->quantita_ordinata}}</p>
                                <p>Venduti: {{$venduto}}</p>
                                <p>Restante: {{$item->quantita_restante}}</p>
                                <div class="progress m-3">
                                    @if ($percent==0)
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" name="bar" role="progressbar"style="width: {{$percent}}%;color:black;" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="{{$item->quantita_restante}}"><span style="color:black">0%</span> </div>
                                    @else
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" name="bar" role="progressbar"style="width: {{$item->quantita_restante}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="{{$item->quantita_restante}}"><span style="color:black">{{$item->quantita_restante}}</span> </div>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </main>
</x-layout>