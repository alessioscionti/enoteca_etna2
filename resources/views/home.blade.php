{{-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> --}}
<style>
  .pagination{
    justify-content: center!important;
  }
  .page-item.active .page-link {
    z-index: 3;
    color: #fff;
    background-color: #11101d!important;
    border-color: #11101d!important;
    border-radius: 10%;
    
}
.page-link{
  text-decoration: none!important;
}
.card:hover{
  transform: none!important;
    
}

</style>
    
<x-layout>    
@if (Auth::user()->admin==1 || Auth::user()->moderator== 1)
    

    <div class="main-content">


      <main>
        <div class="cards">
          <div class="card-single">
            <div>
              @if (Auth::user()->admin==1)
              <h1>{{$customerall}}</h1>
              @else
              <h1>{{$customer}}</h1>
              @endif
              <span>Utenti</span>
            </div>
            <div>
              <span class="fas fa-users"></span>
            </div>
          </div>
          <div class="card-single">
            <div>
              @if (Auth::user()->admin==1)
              <h1>{{$wines}}</h1>
              <span>Vini in cantina</span>
              @else
              <h1>{{$winesrestaurant}}</h1>
              <span>Vini in cantina</span>  
              @endif
            </div>
            <div>
              <span class="fas fa-wine-bottle"></span>
            </div>
          </div>
          <div class="card-single">
            <div>
              @if (Auth::user()->admin==1)
              <h1>{{$orders}}</h1>
              @else
              <h1>{{$ordersrestaurantcount}}</h1> 
              @endif
              <span>Ordini</span>
            </div>
            <div>
              <span class="fas fa-shopping-cart"></span>
            </div>
          </div>
          <div class="card-single">
            @if (Auth::user()->admin==1)
              <div>
                <span>Incassi dalla vendita di vini</span>
                <h2 style="color: green">€ {{$totalguadagnoetna}}</h2>
              </div> 
            @elseif(Auth::user()->moderator==1)
              <div>
                <span>Incassi dalla vendita di vini</span>
                @if ($totalguadagno < $totalespesa)
                <h2 style="color: red">€ {{$totalguadagno}}</h2><hr style="color: white">
                @else
                <h2 style="color: green">€ {{$totalguadagno}}</h2> <hr style="color: white">
                @endif
                <span>Spese dall'acquisto dei vini</span>
                <h2 style="color: white">€ {{$totalespesa}}</h2>
              </div> 
            @endif
              
            <div>
              <span class="fas fa-wallet"></span>
            </div>
          </div>

        </div>
        
        <div class="recent-grid">
          @if (Auth::user()->admin==1)
          <div class="projects">
            <div class="card">
              <div class="card-header">
                <h2>Ordini recenti</h2>
                <a href="{{route('ordini.ordini')}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span></button></a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table width="100%">
                  <thead>
                    <tr>
                      <td>ID ordine</td>
                      <td>Ristorante</td>
                      <td>Data Ordine</td>
                      <td>Stato pagamento</td>
                      <td>Stato spedizione</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($orderadmin as $order)
                    <tr>
                      <td>{{$order->id}}</td>
                      <td>{{$order->restaurant->ragione_sociale}}</td>
                      <td>{{$order->data_ordine}}</td>
                      <td>
                        @if ($order->pagato==1)
                        <span class=""></span><span class="border p-1" style="background-color: green;color:white;border:1px solid green;border-radius:5px;">Pagato</span>
                        @elseif($order->pagato==0)
                        <span class=""></span><span class="border p-1" style="background-color: rgb(255, 166, 0);color:black;border:1px solid rgb(255, 166, 0);border-radius:5px;">Sospeso</span>
                        @else
                        <span class=""></span><span class="border p-1" style="background-color: red;color:white;border:1px solid red;border-radius:5px;">Non pagato</span>
                        @endif
                        
                      </td>
                      <td>
                        @if ($order->evaso==1)
                        <span class=""></span><span class="border p-1" style="background-color: green;color:white;border:1px solid green;border-radius:5px;">Spedito</span>
                        @else
                        <span class=""></span><span class="border p-1" style="background-color: red;color:white;border:1px solid red;border-radius:5px;">Non spedito</span>
                        @endif
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>

                </table>
                </div>
              </div>
            </div>
            <div class="container-xxl mt-1">
              <div class="row justify-content-center">
                <div class="col-12 text-center">
                  {{$orderadmin->links()}}
                </div>
              </div>
            </div>
          </div>
          @else
          <div class="projects">
            <div class="card">
              <div class="card-header">
                <h2>Ordini recenti</h2>
                <a href="{{route('ordini.ordini')}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span></button></a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table width="100%">
                  <thead>
                    <tr>
                      <td>ID ordine</td>
                      <td>Ristorante</td>
                      <td>Data Ordine</td>
                      <td>Stato pagamento</td>
                      <td>Stato spedizione</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($ordersrestaurant as $order)
                    <tr>
                      <td>{{$order->id}}</td>
                      <td>{{$order->restaurant->ragione_sociale}}</td>
                      <td>{{$order->data_ordine}}</td>
                      <td>
                        @if ($order->pagato==1)
                        <span class="status ok"></span><span class="border p-1" style="background-color: green;color:white;border:1px solid green;border-radius:5px;">Pagato</span>
                        @else
                        <span class="status ko"></span><span class="border p-1" style="background-color: red;color:white;border:1px solid green;border-radius:5px;">Non pagato</span>
                        @endif
                        
                      </td>
                      <td>
                        @if ($order->evaso==1)
                        <span class="status ok"></span><span class="border p-1" style="background-color: green;color:white;border:1px solid green;border-radius:5px;">Spedito</span>
                        @else
                        <span class="status ko"></span><span class="border p-1" style="background-color: red;color:white;border:1px solid green;border-radius:5px;">Non spedito</span>
                        @endif
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>

                </table>
                </div>
              </div>
            </div>
            <div class="container-xxl mt-1">
              <div class="row justify-content-center">
                <div class="col-12 text-center">
                  {{$ordersrestaurant->links()}}
                </div>
              </div>
            </div>
          </div>
          @endif
          @if (Auth::user()->admin==1)
          <div class="customers">
            <div class="card">
              <div class="card-header">
                  <h2>Nuovi Utenti</h2>
                  {{-- <a href="{{route('users.users')}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span> </button></a> --}}
              </div>
              <div class="card-body">
                @foreach ($users as $user)
                <div class="customer">
                  <div class="info">
                    @if ($user->immagine!=null)
                    <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" height="40px" width="40px" alt="avatar"></a>
                    @else
                    <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" height="40px" width="40px" alt="customer"></a>
                    @endif
                    <div>
                      <h4>{{$user->name}}</h4>
                      @if ($user->admin==1)
                      <small>Super Admin</small>
                      @elseif($user->moderator==1)
                      <small>Moderatore</small>
                      @elseif($user->admin==0 && $user->moderator==0 && $user->waiters==0)
                      <small>Ruolo non definito</small>
                      @else
                      <small>Cameriere</small>
                      @endif
                      <h4>{{$user->restaurant->ragione_sociale}}</h4>
                    </div>
                  </div>
                  <div class="contact">
                      <span class="fas fa-user-circle"></span>
                      <span class="fas fa-comment"></span>
                      <span class="fas fa-phone-alt"></span>
                    </div>
                </div>
                @endforeach
              </div>
            </div>

          </div>
          @else
          <div class="customers">
            <div class="card">
              <div class="card-header">
                  <h2>Nuovi Utenti</h2>
                  {{-- <a href="{{route('users.users')}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span> </button></a> --}}

              </div>
              <div class="card-body">
                @foreach ($userrestaurant as $user)
                <div class="customer">
                  <div class="info">
                    @if ($user->immagine!=null)
                    <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" height="40px" width="40px" alt="avatar"></a>
                    @else
                    <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" height="40px" width="40px" alt="customer"></a>
                    @endif

                    
                    <div>
                      <h4>{{$user->name}}</h4>
                      @if ($user->admin==1)
                      <small>Super Admin</small>
                      @elseif($user->moderator==1)
                      <small>Moderatore</small>
                      @elseif($user->admin==0 && $user->moderator==0 && $user->waiters==0)
                      <small>Ruolo non definito</small>
                      @else
                      <small>Cameriere</small>
                      @endif
                    </div>
                  </div>
                  <div class="contact">
                      <span class="fas fa-user-circle"></span>
                      <span class="fas fa-comment"></span>
                      <span class="fas fa-phone-alt"></span>
                    </div>
                </div>
                @endforeach
              </div>
            </div>

          </div>
          @endif

          
        </div>  
        @if (Auth::user()->moderator==1)
            
        
        <div class="recent-grid">
          <div class="projects">
            <div class="card">
              <div class="card-header">
                <h2>Vendite recenti</h2>
                <a href="{{-- {{route('ordini.ordini')}} --}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span></button></a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table width="100%">
                  <thead>
                    <tr>
                      <td>Data vendita</td>
                      <td>Vino</td>
                      <td>quantita</td>
                      <td>Cameriere</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($salespaginaterestaurant as $salespag)
                    <tr>
                      <td>{{\Carbon\Carbon::parse($salespag->created_at)->format('d-M-Y')}}</td>
                      <td>{{$salespag->wine->nome}}</td>
                      <td>
                        <span class="status purple"></span>
                        {{$salespag->quantita}}
                      </td>
                      <td>{{$salespag->user->name}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="container-xxl mt-1">
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              {{$salespaginate->links()}}
            </div>
          </div>
      </div>
      </main>
    </div>

    
    @else


    <div class="main-content">


      <main>
        <div class="cards">

          <div class="card-single">
            <div>
              <span>Mie Vendite</span>
              <h1 style="color: #11102d;">{{$sales}}</h1>
            </div>
            <div>
              <span class="fas fa-wallet"></span>
            </div>
          </div>
          <div class="card-single">
            <div>
              <span>Provvigioni Guadagnate</span>
              @if ($guadagno)
              <h1 style="color: green;">{{$guadagno}} €</h1>
              @else
              <h1 style="color: green;">0 €</h1>
              @endif
            </div>
            <div>
              <span class="fas fa-euro-sign"></span>
            </div>
          </div>

        </div>

        <div class="recent-grid">
          <div class="projects">
            <div class="card">
              <div class="card-header">
                <h2>Vendite recenti</h2>
                <a href="{{-- {{route('ordini.ordini')}} --}}"><button>Guarda tutti <span class="fas fa-arrow-right"></span></button></a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table width="100%">
                  <thead>
                    <tr>
                      <td>Data vendita</td>
                      <td>Vino</td>
                      <td>Quantita Bottiglie</td>
                      <td>Quantita Bicchieri</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($salespaginate as $salespag)
                    <tr>
                      <td>{{$salespag->created_at}}</td>
                      <td>{{$salespag->wine->nome}}</td>
                      <td>
                        <span class="status purple"></span>
                        {{$salespag->quantita_bottiglie}}
                      </td>
                      <td>
                        <span class="status purple"></span>
                        {{$salespag->quantita_bicchieri}}
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-xxl mt-1">
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              {{$salespaginate->links()}}
            </div>
          </div>
      </div>
    </div>
      </main>
    </div>   
@endif
</x-layout>
