<style>




.card {
    position: relative;
    width: 500px;
    height: 300px;
    background: rgba(255, 255, 255, 0.1);
    backdrop-filter: blur(10px);
    -webkit-backdrop-filter: blur(10px);
    border-radius: 25px;
    box-shadow: 0 25px 25px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    border-top: 1px solid rgba(255, 255, 255, 0.25);
    transition: 0.5s;
    display: flex;
    justify-content: center;
    align-items: center;
}



</style>

<x-layout>
{{-- <?php dd(isset($filtername)) ?> --}}
<main class="main-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <div class="container-xxl">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('users.new')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;"> c r e a &nbsp; n u o v o &nbsp; U t e n t e </p></a>
            </div>
        </div>
    </div>
    <form action="{{route('filter.user')}}" method="GET">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                    <label for="ristorante">Ristorante</label><br>
                    <input type="text" name="ristorante" placeholder="cerca per nome ristorante" class="form-control" style="height: 1.5em">
                </div>
                <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                    <label for="nome">Nome</label><br>
                    <input type="text" name="nome" placeholder="cerca per nome utente" class="form-control" style="height: 1.5em">
                </div>
                <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                    <label for="role">Ruolo</label><br>
                    <select name="role" class="filter" id="">
                        <option value="" selected>--</option>
                        <option value="1">Tutti i ruoli</option>
                        <option value="admin">Amministratore</option>
                        <option value="moderator">Ristoratore</option>
                    </select>
                </div>
            <div class="row justify-content-center">
                <button type="submit" class="btn btn-dark mt-3">Applica</button>
            </div>
            </div>
        </div>
    </form>
@if(isset($role))
    @if (Auth::user()->admin==1)
        <div class="container-xxl mt-2">
            <div class="row">
                @foreach ($role as $user)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('users.details',$user)}}" style="color:black;text-decoratione:none;">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                            <div class="avatar d-flex justify-content-center">
                                @if ($user->immagine!=null)
                                <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                                @else
                                <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                            <span>Ruolo:</span>
                            @if ($user->admin==1)
                            <span style="font-weight: 900">Super Admin</span>
                            @elseif($user->moderator==1)
                            <span style="font-weight: 900">Ristoratore</span>
                            @else
                            <span style="font-weight: 900">Cameriere</span>
                            @endif
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$role->links()}}
            </div>
            </div>
        </div>
        @else
        <div class="container-xxl mt-2">
            <div class="row justify-content-center">
            @foreach ($rolemoderator as $user)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                        <div class="avatar d-flex justify-content-center">
                            @if ($user->immagine!=null)
                            <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                            @else
                            <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                            @endif
                    </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                        <span>Ruolo:</span>
                        @if ($user->admin==1)
                        <span style="font-weight: 900">Super Admin</span>
                        @elseif($user->moderator==1)
                        <span style="font-weight: 900">Ristoratore</span>
                        @else
                        <span style="font-weight: 900">Cameriere</span>
                        @endif
                    </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$rolemoderator->links()}}
            </div>
            </div>
        </div>

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        @endif
@elseif(isset($restaurant))

    @if (Auth::user()->admin==1)
        <div class="container-xxl mt-2">
            <div class="row">
                @foreach ($restaurant as $user)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('users.details',$user)}}" style="color:black;text-decoratione:none;">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                            <div class="avatar d-flex justify-content-center">
                                @if ($user->immagine!=null)
                                <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                                @else
                                <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                            <span>Ruolo:</span>
                            @if ($user->admin==1)
                            <span style="font-weight: 900">Super Admin</span>
                            @elseif($user->moderator==1)
                            <span style="font-weight: 900">Ristoratore</span>
                            @else
                            <span style="font-weight: 900">Cameriere</span>
                            @endif
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$restaurant->links()}}
            </div>
            </div>
        </div>
        @else
        <div class="container-xxl mt-2">
            <div class="row justify-content-center">
            @foreach ($restaurantmoderator as $user)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                        <div class="avatar d-flex justify-content-center">
                            @if ($user->immagine!=null)
                            <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                            @else
                            <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                            @endif
                        </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                        <span>Ruolo:</span>
                        @if ($user->admin==1)
                        <span style="font-weight: 900">Super Admin</span>
                        @elseif($user->moderator==1)
                        <span style="font-weight: 900">Ristoratore</span>
                        @else
                        <span style="font-weight: 900">Cameriere</span>
                        @endif
                    </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$restaurantmoderator->links()}}
            </div>
            </div>
        </div>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    @endif
@elseif(isset($filtername))
@if (Auth::user()->admin==1)
<div class="container-xxl mt-2">
    <div class="row">
        @foreach ($filtername as $user)
        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
        <a href="{{route('users.details',$user)}}" style="color:black;text-decoratione:none;">
            <div class="card m-3">
            <div class="content">
                <div class="row justify-content-center">
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                    <div class="avatar d-flex justify-content-center">
                        @if ($user->immagine!=null)
                        <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                        @else
                        <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                <div class="details">
                <div>
                    <h3 style="font-weight: 900">{{$user->name}}</h3>
                </div>
                <div>
                    <ion-icon name="mail-outline"></ion-icon>
                    <span style="font-weight: 900">{{$user->email}}</span>
                </div>
                <div>
                    <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                </div>
                <div>
                    <span>Ruolo:</span>
                    @if ($user->admin==1)
                    <span style="font-weight: 900">Super Admin</span>
                    @elseif($user->moderator==1)
                    <span style="font-weight: 900">Ristoratore</span>
                    @else
                    <span style="font-weight: 900">Cameriere</span>
                    @endif
                </div>
                </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    @endforeach
</div>
</div>
<div class="container-xxl mt-3">
    <div class="row justify-content-center">
    <div class="col-12 text-center mb-5">
        {{$filtername->links()}}
    </div>
    </div>
</div>
@else
<div class="container-xxl mt-2">
    <div class="row justify-content-center">
    @foreach ($filternamemoderator as $user)
    <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
        <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none">
            <div class="card m-3">
            <div class="content">
                <div class="row justify-content-center">
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <div class="avatar d-flex justify-content-center">
                    @if ($user->immagine!=null)
                    <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                    @else
                    <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                    @endif
                </div>
                </div>
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                <div class="details">
                <div>
                    <h3 style="font-weight: 900">{{$user->name}}</h3>
                </div>
                <div>
                    <ion-icon name="mail-outline"></ion-icon>
                    <span style="font-weight: 900">{{$user->email}}</span>
                </div>
                <div>
                    <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                </div>
                <div>
                <span>Ruolo:</span>
                @if ($user->admin==1)
                <span style="font-weight: 900">Super Admin</span>
                @elseif($user->moderator==1)
                <span style="font-weight: 900">Ristoratore</span>
                @else
                <span style="font-weight: 900">Cameriere</span>
                @endif
            </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
    @endforeach
</div>
</div>
<div class="container-xxl mt-3">
    <div class="row justify-content-center">
    <div class="col-12 text-center mb-5">
        {{$filternamemoderator->links()}}
    </div>
    </div>
</div>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
@endif

@else
    @if (Auth::user()->admin==1)
        <div class="container-xxl mt-2">
            <div class="row">
                @foreach ($seladmin as $user)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('users.details',$user)}}" style="color:black;text-decoratione:none;">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                            <div class="avatar d-flex justify-content-center">
                                @if ($user->immagine!=null)
                                <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                                @else
                                <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                            <span>Ruolo:</span>
                            @if ($user->admin==1)
                            <span style="font-weight: 900">Super Admin</span>
                            @elseif($user->moderator==1)
                            <span style="font-weight: 900">Ristoratore</span>
                            @else
                            <span style="font-weight: 900">Cameriere</span>
                            @endif
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$seladmin->links()}}
            </div>
            </div>
        </div>
        @else
        <div class="container-xxl mt-2">
            <div class="row justify-content-center">
            @foreach ($seladminrestaurant as $user)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                        <div class="avatar d-flex justify-content-center">
                            @if ($user->immagine!=null)
                            <a href="{{route('users.details',$user)}}"> <img src="{{asset("storage/user/".$user->id."/".$user->id.".jpg")}}" alt="avatar"></a>
                            @else
                            <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                            @endif
                        </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$user->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$user->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                        <span>Ruolo:</span>
                        @if ($user->admin==1)
                        <span style="font-weight: 900">Super Admin</span>
                        @elseif($user->moderator==1)
                        <span style="font-weight: 900">Ristoratore</span>
                        @else
                        <span style="font-weight: 900">Cameriere</span>
                        @endif
                    </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
            @endforeach
        </div>
        </div>
        <div class="container-xxl mt-3">
            <div class="row justify-content-center">
            <div class="col-12 text-center mb-5">
                {{$seladminrestaurant->links()}}
            </div>
            </div>
        </div>

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    @endif
@endif
<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous">
</script>
</main>
</x-layout>