<style>
  input[type=checkbox] {
    transform: scale(1.5);
    margin-right: 1rem;
}
.login-form {

    background: #fefeffc9;
    margin: auto;
    position: absolute;
    left: 0;
    right: 0;
    top: 20%!important;
    border-radius: 10px;
}
</style>
<x-layout>
    <div style="height: 100px"></div>

      <div class="container-xxl">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            <h1 id='title'>Registrazione Nuovo Utente</h1>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
      </div>
      <div class="container-xxl">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
        <main id='main' class="mt-5">
          <p id='description'></p>
          <figure class="login-form shadow">
            <form id='survey-form' method="POST" action="{{route('users.store')}}"enctype="multipart/form-data">
              @csrf
              <label id='name-label'><span style="font-weight:900;">Nome</span></label>
              <input type='text' id='name' name="name" placeholder='nome' class='form-control' required>
              
              <label id='name-label'><span style="font-weight:900;">Cognome</span></label>
              <input type='text' id='name' name="cognome" placeholder='cognome' class='form-control' required>

              <div class="form-group">
                <label id='email-label'><span style="font-weight:900;">Email</span></label>
                <input type='email' id='email' placeholder='Email' class='form-control' name="email" required>
              </div>
              
              <div class="form-group">
                <label id='number-label'><span style="font-weight:900;">Password</span></label>
                <input type='password' min='5' max='110' class='form-control' placeholder='password..' name="password" required>
              </div>
              
              <div class="form-group">
                <label id='number-label'><span style="font-weight:900;">Conferma Password</span></label>
                <input type='password' min='5' max='110' class='form-control' placeholder='password..' name="password_confirmation" required>
              </div>
              <div class="form-group">
                <label id='email-label'>Immagine</label>
                <input type='file' id='email' class='form-control' name="immagine">
              </div>
              <div class="associa form-group text-start">
                <div class="text-center">
                  <label id='number-label'><span style="font-weight:900;">L'utente è un?</span></label>
                </div>
                @if (Auth::user()->admin==1)
                <input type='checkbox' name="admin" value="1">Admin <br>
                @endif
                <input type='checkbox' name="moderator" value="1" class="moderator">Ristoratore(se non presente creare prima il ristorante) <br>


                <div class="container if-exist form-group" value="1" style="margin-top: 10px; margin-left:3rem" hidden>
                  <div class="row justify-content-center">
                      <div class="col-12 text-start">
                        <p>Ristorante gia esistente?</p>
                        <input type='checkbox' name="si" class="si" value="1">Si <br>
                        <input type='checkbox' name="no" value="1" class="no">No <br>
                      </div>
                  </div>
                </div>


                <div class="select-restaurant mb-3 form-group" hidden>
                  <label for="">Associa Ristorante1</label>
                  <select name="restaurant1">
                    <option value="0" selected disabled>Seleziona</option>
                    @if (Auth::user()->admin==1)

                      @foreach ($restaurants as $restaurant)
                      <option value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                      @endforeach
                    @else
                      @foreach ($restaurantslogin as $restaurant)
                      <option value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                      @endforeach
                    @endif
                  </select>  
                </div>

                <div class="container new-restaurant form-group" value="1" style="margin-top: 10px; margin-left:3rem" hidden>
                    <div class="row justify-content-center">
                        <div class="col-12 text-center">
                            <a href="{{route('restaurant.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> c r e a &nbsp; r i s t o r a n t e</p></a>
                        </div>
                    </div>
                </div>            
                <input type='checkbox' name="waiters" value="1" class="waiters">Cameriere
                <hr>
              </div>
              <div class="waiter mb-3 form-group" hidden>
                <label for="">Associa Ristorante</label>
                <select name="restaurant" required>
                    <option value="0" selected disabled>Seleziona</option>
                    @if (Auth::user()->admin==1)
                      @foreach ($restaurants as $restaurant)
                      <option value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                      @endforeach
                    @else
                      @foreach ($restaurantslogin as $restaurant)
                      <option value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                      @endforeach 
                    @endif
                </select>  
              </div>
              
              <button class="btn btn-success" id="submit">Registra Utente</button>
        
            </form>
          </figure>
        </main>
      </div>
    </div>
  </div>
<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous"></script>


<script>

$(".waiters").on("click", function () { 
   console.log("click!!! address2");
   var $cameriere = $(this).find(':checkbox');
   var checkBoxes = $("input[name='waiters']");
   if(checkBoxes.prop("checked")==true)
   $('.waiter').attr('hidden', false);
   else
   $('.waiter').attr('hidden', true);
});

$(".moderator").on("click", function () { 
   console.log("click!!! address2");
   var $restaurant = $(this).find(':checkbox');
   var checkBoxes = $("input[name='moderator']");
   if(checkBoxes.prop("checked")==true){

     $('.if-exist').attr('hidden', false);
   }else{

     $('.if-exist').attr('hidden', true);
     $('.select-restaurant').attr('hidden', true);
     $('.new-restaurant').attr('hidden', true);
   }
   
});

$(".si").on("click", function () { 
   console.log("click!!! address2");
   var $restaurant = $(this).find(':checkbox');
   var checkBoxes = $("input[name='si']");

   if(checkBoxes.prop("checked")==true){
     $('.if-exist').attr('hidden', true);
     $('.select-restaurant').attr('hidden', false);

   }else{
     $('.if-exist').attr('hidden', true);
     $('.select-restaurant').attr('hidden', true);
     $('.new-restaurant').attr('hidden', true);
   }
   
   
});
$(".no").on("click", function () { 
   console.log("click!!! address2");
   var $restaurant = $(this).find(':checkbox');
   var checkBoxes = $("input[name='no']");

   if(checkBoxes.prop("checked")==true){
     $('.if-exist').attr('hidden', true);
     $('.new-restaurant').attr('hidden', false);

   }else{
     $('.if-exist').attr('hidden', false);
     $('.new-restaurant').attr('hidden', true);
   }
   
   
});



  </script>
  
  </x-layout>