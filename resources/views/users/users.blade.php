<style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Poppins', sans-serif;
}



.card {
    position: relative;
    width: 500px;
    height: 300px;
    background: rgba(255, 255, 255, 0.1);
    backdrop-filter: blur(10px);
    -webkit-backdrop-filter: blur(10px);
    border-radius: 25px;
    box-shadow: 0 25px 25px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    border-top: 1px solid rgba(255, 255, 255, 0.25);
    transition: 0.5s;
    display: flex;
    justify-content: center;
    align-items: center;
}

.card:hover {
    transform: scale(1.1);
    background: rgba(255, 255, 255, 0.25);
}



.card .content {
    position: relative;
    display: flex;
    align-items: center;
    margin-top: -80px;
    gap: 25px;
}

.card .content .avatar {
    position: relative;
    width: 120px;
    height: 120px;
    overflow: hidden;
    border-radius: 20%;
    margin-top: 4rem;
}

.card .content .avatar img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
}


.card .content .details div {
    display:  flex;
    align-items: center;
    gap: 10px;
}



.card .content .details ion-icon {
    font-size: 1.5em;
}
</style>

<x-layout>
    

    <div class="container-xxl" style="margin-top: 100px">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('users.new')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> c r e a &nbsp; n u o v o &nbsp; U t e n t e </p></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <div class="container-xxl button-role">
        <div class="row justify-content-center">
            <div class="col-12 text-center" style="margin-left:-7rem;">
                <button class="btn btn-success admin"  style="border:1px solid #fafafc;margin-right:-2px;border-top-left-radius:10px;border-bottom-left-radius:10px;border-top-right-radius:0px;border-bottom-right-radius:0px;width:150px;background-color:#11102d"><a href="{{route('users.showadmin')}}" style="color:white;text-decoration:none">Admin/mod</a></button>
                <input type="text" class="sel-admin" value="1" hidden>
                <button class="btn btn-success waiters"  style="border:1px solid #fafafc;margin-left:-2px;border-top-right-radius:10px;border-bottom-right-radius:10px;border-top-left-radius:0px;border-bottom-left-radius:0px;width:150px;background-color:#11102d"><a href="{{route('users.showwaiters')}}" style="color:white;text-decoration:none">Camerieri</a></button>
                <input type="text" class="sel-waiters" value="1" hidden>
            </div>
        </div>
    </div>
    <div class="container-xxl mt-2">
        <div class="row">
        @foreach ($users as $user)
        <div class="col">
        <a href="{{route('users.details',$user)}}">
            <div class="card m-3">
            <div class="content">
                <div class="avatar">
                <a href="{{route('users.details',$user)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                </div>
                <div class="details">
                <div>
                    <h3 style="font-weight: 900">{{$user->name}}</h3>
                </div>
                <div>
                    <ion-icon name="mail-outline"></ion-icon>
                    <span style="font-weight: 900">{{$user->email}}</span>
                </div>
                <div>
                    <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                </div>
                <div>
                    <span>Ruolo:</span>
                    @if ($user->admin==1)
                    <span style="font-weight: 900">Super Admin</span>
                    @elseif($user->moderator==1)
                    <span style="font-weight: 900">Ristoratore</span>
                    @else
                    <span style="font-weight: 900">Cameriere</span>
                    @endif
                  </div>
                </div>
            </div>
            </div>
        </div>
        @endforeach
    </div>
      </div>
      <div class="container-xxl mt-3">
        <div class="row justify-content-center">
          <div class="col-12 text-center mb-5" style="margin-left:-7rem;">
            {{$users->links()}}
          </div>
        </div>
    </div>
      <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
      <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous">
</script>

{{-- <script>
    $('.admin').click(function (e) { 
        e.preventDefault();
        
        let admin=$(this).closest('.button-role').find('.sel-admin').val();
        

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "GET",
                url: "/users/show-admin",
                data: {
                    'admin':admin,
                    
                },
                success: function (response) {
                    console.log(response);
                }
            });

    });
    $('.waiters').click(function (e) { 
        e.preventDefault();
        
        let waiters=$(this).closest('.button-role').find('.sel-waiters').val();
        

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "GET",
                url: "/users/show-waiters",
                data: {
                    'waiters':waiters,
                    
                },
                success: function (response) {
                    
                }
            });

    });
</script> --}}
</x-layout>