<style>
    button:not(:disabled), [type=button]:not(:disabled), [type=reset]:not(:disabled), [type=submit]:not(:disabled) {
    cursor: pointer;
    margin-left: 5rem;
}

    input, textarea:focus, select:focus{
        outline: none!important;
        background: transparent!important;
        border: none!important;
        color: green;
        font-weight: 900;
    }

</style>
<x-layout>
<main class="main-content">
    <div class="container-xxl p-5">
    <form action="{{route('ordini.store')}}" method="POST">
        @csrf
    @if (!$cart->isEmpty())
    <table class="table border rounded bg-light shadow-lg">
        <thead style="background-color: #11101d;color:white">
            <tr>
                <th class="p-1 text-center">Immagine</th>
                <th class="p-1 text-center">Nome</th>
                <th class="p-1 text-center">Prezzo a bottiglia</th>
                <th class="p-1 text-center">Prezzo totale</th>
                <th class="p-1 text-center">Quantita</th>
                <th class="p-1 text-center">Azioni</th>
            </tr>
        </thead>
        <div class="row mt-5 justify-content-center align-items-center">
            <tbody>
                @foreach ($cart as $item)
                <input type="text" name="wine_id" value="{{$item->id}}" hidden>
                <input type="text" value="{{$item->wine->prezzo_bottiglia}}"hidden>
                <input name="wine[{{$item->wine->id}}]" type="text" value="{{$item->wine->id}}"hidden>
                <tr class="product-table">
                    <td><img src="https://www.tascapan.com/wp-content/uploads/2014/07/Nus-Retro.jpg" class="card-img-top" alt="..." style="width:150px;heigth:150px;"></td>
                    <td><h4 class="align-items-center mt-5 text-center">{{$item->wine->nome}}</h4></td>
                    <td><h4 class="mt-5 text-center"><input class="prezzobottiglia text-end" type="text" value="{{$item->wine->prezzo_bottiglia}}" style="border:none;width:30%">€</h4></td>
                    <td><h4 class="mt-5 text-center"><input class="prezzotot text-end" type="text" value="" style="border:none;width:25%">€</h4></td>
                    <td>
                        <div class="col-12 mt-5">
                            <button type="button" class="btn btn-danger decrement-btn" style="width: 27px;height:27px;margin-right:-1px;"><p>-</p></button>
                            <input type="number" class="dec_1" value="1" hidden>
                            <input type="number" class="inc_1" value="1" hidden>
                            <input type="number" class="id-wine" value="{{$item->wine->id}}" hidden>
                            <input type="int" min="1" name="quantity[{{$item->wine->id}}]" class="quantity rounded text-center" value="{{$item->wine_qty}}" id="qty" readonly style="min-width: 20%;border:1px solid grey;max-width: 20%">
                            <button type="button" class="btn btn-success increment-btn" style="width: 27px;height:27px;margin-left:-1px;">+</button>  
                        </div>  
                    </td>
                    <td><button class="btn btn-danger delete m-5" value="{{$item->id}}" style="margin-top:1.1rem;">X</button></td>
                </tr>
                @endforeach
            </tbody>
        </div>
    </table>

    
    @if (Auth::user()->admin==1)
    <div class="container mt-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2>Vuoi assegnare questo ordine a un ristorante?</h2>
                <select name="restaurant" id="">
                    <option value="0" selected>--</option>
                    @foreach ($restaurants as $restaurant)
                    <option value="{{$restaurant->id}}">{{$restaurant->ragione_sociale}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @endif
    <div class="container mt-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-success">Procedi all'ordine</button>
            </div>
        </div>
    </div>
</form>
<?php 
foreach ($cart as $key) {
    $idorder=$key->id;
}
?>

    @else
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {!!Session('message')!!}
                </div>
            @endif
            </div>
        </div>
    </div>
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Carrello vuoto</h1>
            </div>
            <div class="col-12 mt-5 text-center">
                <a class="btn btn-warning" href="{{route('ordini.create')}}" style="color:#11101d">Crea un ordine</a>
            </div>
        </div>    
    @endif        
</div>
<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous">
</script>

<script>
    $(document).ready(function () {

        $('.increment-btn').click(function (e) { 
            e.preventDefault();
            let totarray=[];
            let inc_val=$(this).closest('.product-table').find('.quantity').val();
            let id_wine=$(this).closest('.product-table').find('.id-wine').val();
            let inc_1=$(this).closest('.product-table').find('.inc_1').val();
            let prezzobottiglia=$(this).closest('.product-table').find('.prezzobottiglia').val();
            console.log(prezzobottiglia);
            let value= parseInt(inc_val,10);
            value= isNaN(value) ? 0: value;
            if (value<1000) {
                value++;
                $(this).closest('.product-table').find('.quantity').val(value);
                let prezzotot=prezzobottiglia*value;
                let tot=totarray.push(prezzotot);
                console.log(totarray);
                $(this).closest('.product-table').find('.prezzotot').val(prezzotot);
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "/increment",
                data: {
                    'inc_1':inc_1,
                    'id_wine':id_wine,
                },
                success: function (response) {
                    
                }
            });
            }


        });
        $('.decrement-btn').click(function (e) { 
            e.preventDefault();
            let dec_val=$(this).closest('.product-table').find('.quantity').val();
            let dec_1=$(this).closest('.product-table').find('.dec_1').val();
            let prezzobottiglia=$(this).closest('.product-table').find('.prezzobottiglia').val();
            console.log(dec_1);
            let id_wine=$(this).closest('.product-table').find('.id-wine').val();
            let value= parseInt(dec_val,10);
            value= isNaN(value) ? 0: value;
            if (value>=1) {
                value--;
               $(this).closest('.product-table').find('.quantity').val(value);
               let prezzotot=prezzobottiglia*value;
                console.log(prezzotot);
                $(this).closest('.product-table').find('.prezzotot').val(prezzotot);
               $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "/decrement",
                data: {
                    'dec_1':dec_1,
                    'id_wine':id_wine,
                },
                success: function (response) {
                    
                }
            });
            }
    
        });

        $('.delete').click(function (e) { 
            e.preventDefault();
            let delete_item=$(this).closest('.product-table').find('.delete').val();
            
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "/delete",
                data: {
                    'delete_item':delete_item,
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        });
    });
</script>
</main>
</x-layout>