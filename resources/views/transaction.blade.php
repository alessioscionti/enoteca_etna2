<x-layout>
    <main class="main-content">
        @if(\Session::has('error'))
        <div class="alert alert-danger">{{ \Session::get('error') }}</div>
        {{ \Session::forget('error') }}
        @endif
        <div class="container-xxl mt-5">
            <div class="row justify-content-center">
                <div class="col text-center">
                    <h2>Grazie!</h2><br><br>
                    <h3>Pgamento effettuato con successo</h3>
                    <h3>Puoi trovare il dettaglio del tuo ordine su <br><br>
                    <a href="{{route('ordini.ordini')}}">I TUOI ORIDINI</a></h3>
                </div>
            </div>
        </div>
    </main>
</x-layout>