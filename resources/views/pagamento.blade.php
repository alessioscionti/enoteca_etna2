<x-layout>
    <?php 
        $id=$order->id;
        $total=$order->prezzo
        ?>
    <main class="main-content">
        <form action="{{ route('processTransaction') }}" method="POST">
            @csrf
        
    @if(\Session::has('error'))
        <div class="alert alert-danger">{{ \Session::get('error') }}</div>
        {{ \Session::forget('error') }}
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success">{{ \Session::get('success') }}</div>
        {{ \Session::forget('success') }}
    @endif
    <input type="text" name="order_id" value="{{$order->id}}" hidden>
    <button class="btn btn-primary m-3" type="submit">paga {{$total}} €</a>
</form>
    </main>
</x-layout>