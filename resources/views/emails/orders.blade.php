<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ordine</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    
       
    <h1>Nuovo Ordine effettuato</h1>
    <h3>ID Ordine: <span style="color: green">{{$order->id}}</span></h3>
    <h3>Ragione Sociale: <span style="color: green">{{$order->restaurant->ragione_sociale}}</span></h3>
    <h3>Data Ordine: <span style="color: green">{{$order->data_ordine}}</span></h3>
    <h3>Costo: <span style="color: green">{{$order->prezzo}} €</span></h3>
    <h4>Hai ordinato:</h4>
    @foreach ($winecount as $item)
        <div class="col pt-5 pb-5 mt-3">
            <h4 style="color: green">{{$item->qty}}x <span style="color: black">{{$item->wine->nome}}</span></h4>
        </div>
    @endforeach
    <p>* troverai il tuo ordine nella sezione "Ordini" dell'App</p>
    
</body>
