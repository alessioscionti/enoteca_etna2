<style>

.card {
    position: relative;
    width: 500px;
    height: 300px;
    background: rgba(255, 255, 255, 0.1);
    backdrop-filter: blur(10px);
    -webkit-backdrop-filter: blur(10px);
    border-radius: 25px;
    box-shadow: 0 25px 25px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    border-top: 1px solid rgba(255, 255, 255, 0.25);
    transition: 0.5s;
    display: flex;
    justify-content: center;
    align-items: center;
}


</style>

<x-layout>
<main class="main-content">   

    <div class="container-xxl">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('users.new')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;"> c r e a &nbsp; n u o v o &nbsp; U t e n t e </p></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    @if (Auth::user()->admin==1)
    <div class="container-xxl mt-2">
        <div class="row">
            @foreach ($waiters as $waiter)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                
                <a href="{{route('waiters.details',$waiter)}}" style="color:black;text-decoratione:none;">
                    <div class="card m-3">
                    <div class="content">
                        <div class="row justify-content-center">
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                        <div class="avatar d-flex justify-content-center">
                            <a href="{{route('waiters.details',$waiter)}}"> <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                        </div>
                        </div>
                        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                        <div class="details">
                        <div>
                            <h3 style="font-weight: 900">{{$waiter->name}}</h3>
                        </div>
                        <div>
                            <ion-icon name="mail-outline"></ion-icon>
                            <span style="font-weight: 900">{{$waiter->email}}</span>
                        </div>
                        <div>
                            <span>ristorante: <span style="font-weight: 900">{{$waiter->restaurant->ragione_sociale}}</span></span>
                        </div>
                        <div>
                            <span>Ruolo:</span>
                            <span style="font-weight: 900">Cameriere</span>
                        </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </a>
            
        </div>
            @endforeach
        </div>
    </div>
    <div class="container-xxl mt-3">
        <div class="row justify-content-center">
          <div class="col-12 text-center mb-5">
            {{$waiters->links()}}
          </div>
        </div>
    </div>  
    @else
    <div class="container-xxl mt-2">
        <div class="row justify-content-center">
        @foreach ($waitersrestaurant as $user)
        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
        <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none">
            <div class="card m-3">
            <div class="content">
                <div class="row justify-content-center">
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <div class="avatar d-flex justify-content-center">
                    <a href="{{route('waiters.details',$user)}}"  style="color:black;text-decoration:none"><img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar"></a>
                </div>
                </div>
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7">
                <div class="details">
                <div>
                    <h3 style="font-weight: 900">{{$user->name}}</h3>
                </div>
                <div>
                    <ion-icon name="mail-outline"></ion-icon>
                    <span style="font-weight: 900">{{$user->email}}</span>
                </div>
                <div>
                    <span>ristorante: <span style="font-weight: 900">{{$user->restaurant->ragione_sociale}}</span></span>
                </div>
                <div>
                    <span>Ruolo:</span>
                    <span style="font-weight: 900">Cameriere</span>
                </div>
                </div>
                </div>
            </div>
            </div>
            </div>
        </div>
        @endforeach
    </div>
      </div>
      <div class="container-xxl mt-3">
        <div class="row justify-content-center">
          <div class="col-12 text-center mb-5">
            {{$waitersrestaurant->links()}}
          </div>
        </div>
    </div> 
    @endif
</main> 
</x-layout>