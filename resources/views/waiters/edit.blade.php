
<x-layout>
    <main class=" main-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>

<div class="container-xxl">
    <div class="row justify-content-center">
        <div class="col-2 d-flex justify-content-center mb-5">
            <a class="fas fa-arrow-circle-left" href="{{ url()->previous() }}" style="font-size: 3rem;color:#dc3545"></a>
        </div>
        <div class="col-10 d-flex justify-content-center mb-5">
            <h1>Modifica Cameriere</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <div class="avatar">
                <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar" style="min-width:80%;max-width:80%">
            </div>     
        </div>
        <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex  justify-content-center">
            <figure>
                <form id='survey-form' method="POST" action="{{route('waiters.update',$user)}}">
                  @csrf
                  <input type="text" name="user_id" value="{{$user->id}}" hidden>
                  <label id='name-label'><span style="font-weight: 900;font-size:2rem;">Nome</span></label><br>
                  <input type='text' id='name' name="name" placeholder='Nome' class='control rounded border' value="{{$user->name}}" required>
                  <hr>
                  @if ($cognome==null)
                  <label id='name-label'><span style="font-weight: 900;font-size:2rem;">Cognome</label></span><br>
                  <input type='text' id='name' name="cognome" placeholder='Cognome' class='control rounded border' value="">
                  @else
                  <label id='name-label'><span style="font-weight: 900;font-size:2rem;">Cognome</label></span><br>
                  <input type='text' id='name' name="cognome" placeholder='Cognome' class='control rounded border' value="{{$cognome}}">
                  @endif
                  <hr>
                  <label id='name-label'><span style="font-weight: 900;font-size:2rem;">Email</label></span><br>
                  <input type='text' id='name' name="email" placeholder='email' class='control rounded border text-center' value="{{$user->email}}" required>
                  <hr>
                  <label id='name-label'><span style="font-weight: 900;font-size:2rem;">% di guadagno (+specificare la percentuale di guadagno sulla vendita di una singola bottiglia)</label></span><br>
                  <input type='text' id='name' name="percentuale" placeholder='email' class='control rounded border text-center' value="" required>
                  <hr>
                  <label id='number-label'><span style="font-weight: 900;font-size:2rem;">Admin</label></span>
                  @if ($user->admin==1)
                  <input type='checkbox' id='number' class='control rounded border text-center' name="admin" checked style="width:20px;height:20px">
                  @else
                  <input type='checkbox' id='number' class='control rounded border text-center' name="admin"style="width:20px;height:20px">
                  @endif
                  <br>
                  <label id='number-label'><span style="font-weight: 900;font-size:2rem;">Moderatore</label></span>
                  @if ($user->moderator==1)
                  <input type='checkbox' id='number' class='control rounded border text-center' name="moderator" checked style="width:20px;height:20px">
                  @else
                  <input type='checkbox' id='number' class='control rounded border text-center' name="moderator" style="width:20px;height:20px">
                  @endif
                  <br>
                  <label id='email-label'><span style="font-weight: 900;font-size:2rem;">Cameriere</label></span>
                  @if ($user->waiters==1)
                  <input type='checkbox' id='email' class='control rounded border text-center' name="waiters" checked style="width:20px;height:20px">
                  @else
                  <input type='checkbox' id='email' class='control rounded border text-center' name="waiters" style="width:20px;height:20px">
                  @endif
                  <hr>
                  <label id='number-label'><span style="font-weight: 900;font-size:2rem;">Associa ristorante </label></span><br>
                  <select name="id_restaurant" id="" class=" rounded border text-center">
                      <option value="" disabled>attualmente associato a ⇓</option>
                      <option value="{{$user->id_restaurant}}" selected>➤ {{$user->restaurant->ragione_sociale}}</option>
                      <option value="" disabled>---</option>
                      @foreach ($restaurant as $rest)
                          <option value="{{$rest->id}}">{{$rest->ragione_sociale}}</option>
                      @endforeach
                  </select><br>
                  <button class='btn btn-success mt-5'id="submit">Salva</button>
            
                </form>
              </figure>      
        </div>
    </div>
</div>

</main>
</x-layout>