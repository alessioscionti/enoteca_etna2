
<x-layout>
<main class=" main-content">
    <div class="container">
        <div class="row align-item-start">
            <div class="col-12">
                <a class="fas fa-arrow-circle-left" href="{{route('users.showadmin')}}" style="font-size: 3rem;color:#dc3545;"></a>
            </div>
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xl-11 col-xxl-11">
                <div class="card" style="max-width: 350px;min-width:350px">
                     <img src="https://www.pngarts.com/files/10/Default-Profile-Picture-PNG-Background-Image.png" alt="avatar" style="width:250px;height:250px;margin-left:2rem;">
                    <div class="mt-4 my-5" style="margin-left: 1rem;">
                      <div>
                        <h3>{{$user->name}}</h3>
                      </div>
                      <div>
                        <h3>Ristorante: <span>{{$user->restaurant->ragione_sociale}}</span></h3>
                      </div>
                      <div>
                        <h3>Ruolo: 
                        <span>Cameriere</span>
                        </h3>
                      </div>
                      
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
            <a href="{{route('users.edit',$user->id)}}"><button class="btn btn-danger mt-2">Modifica</button></a>
            </div>
        </div>
        
      </div>
</main>
</x-layout>