<style>
.card {
    transition: 1s;

}

</style>

<x-layout>
    <main class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('restaurant.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> c r e a &nbsp; r i s t o r a n t e</p></a>
            </div>
        </div>
    </div>
    
    <div class="container-xxl mt-5">
        <div class="row">
        @foreach ($restaurants as $restaurant)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <div class="card m-3">
                    <div class="content">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                <h3>{{$restaurant->ragione_sociale}}</h3>
                            </div>
                            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                <div class="avatar d-flex justify-content-center">
                                    <a href="{{-- {{route('restaurant.details',$user)}} --}}"><img src="{{asset('storage/img/rifugio_sapienza.jpg')}}" alt=""></a>
                                </div>
                            </div>
                            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                <div class="details">
                                    <div>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <span>Indirizzo: <span style="font-weight: 600;color:green">{{$restaurant->indirizzo}}</span></span>
                                    </div>
                                    <div>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <span>Città: <span style="font-weight: 600;color:green">{{$restaurant->citta}}</span></span>
                                    </div>
                                    <div>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <span>Provincia: <span style="font-weight: 600;color:green">{{$restaurant->provincia}}</span></span>
                                    </div>
                                    <div>
                                        <i class="fas fa-building"></i>
                                        <span>Partita iva: <span style="font-weight: 600;color:green">{{$restaurant->partita_iva}}</span></span>
                                    </div>
                                    <div>
                                        <i class="fa-solid fa-envelope"></i>
                                        <span>Pec: <span style="font-weight: 600;color:green">{{$restaurant->pec}}</span></span>
                                    </div>
                                    <div>
                                        <i class="fa-solid fa-phone"></i>
                                        <span>telefono: <span style="font-weight: 600;color:green">{{$restaurant->telefono}}</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</main>
</x-layout>