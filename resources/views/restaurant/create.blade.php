<style>

.login-form {
    width: 550px!important;
    padding: 40px 30px;
    background: #fefeffc9;
    margin: auto;
    position: absolute;
    left: 0;
    right: 0;
    top: 20%!important;
    border-radius: 10px;
}
main {
    margin-top: 85px;
    padding: 2rem 1.5rem;
    background: linear-gradient(rgba(255, 255, 255, .8), rgba(255, 255, 255, .8)), url(https://vininativ.it/wp-content/uploads/2020/04/2768481.jpg) 50%/ cover #eee;
    background-blend-mode: luminosity;
    height: 100vh;
}
</style>
<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
  <main class="main-content">
      <div class="container-xxl">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            <h1 id='title' style="margin-left:-4.5rem;">Inserisci Nuovo Ristorante</h1>
          </div>
        </div>
      </div>
      <div class="container-xxl">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
              <figure class="login-form shadow text-center">
                <form id='survey-form' method="POST" action="{{route('restaurant.store')}}">
                  @csrf
                  <label id='name-label'><span style="font-weight:900;">Ragione Sociale</span></label>
                  <input type='text' id='name' name="ragione_sociale" placeholder='ragione sociale' class='form-control' required>
                  
                  <label id='name-label'><span style="font-weight:900;">Indirizzo</span></label>
                  <input type='text' id='name' name="indirizzo" placeholder='indirizzo' class='form-control' required>
                  
                  <label id='number-label'><span style="font-weight:900;">Città</span></label>
                  <input type='text' id='number' placeholder='Inserisci Città' class='form-control' name="citta">
                  
                  <label id='number-label'><span style="font-weight:900;">Provicia</span></label>
                  <input type='text' id='number' placeholder='Inserisci Provincia' class='form-control' name="provincia">
                  
                  <label id='number-label'><span style="font-weight:900;">Pec</span></label>
                  <input type='email' id='number' placeholder='Inserisci pec' class='form-control' name="pec">

                  <label id='email-label'><span style="font-weight:900;">CAP</span></label>
                  <input type='number' id='email' placeholder='Inserisci CAP'  class='form-control' name="cap" required>
                  
                  <label id='number-label'><span style="font-weight:900;">Partita Iva</span></label>
                  <input type='text' id='number' min='5' max='110' class='form-control' placeholder='partita iva' name="partita_iva" required>

                  <label id='email-label'><span style="font-weight:900;">Codice Univoco</span></label>
                  <input type='number' id='email' placeholder='Codice Univoco..' class='form-control' name="cod" required>

                  <label id='email-label'><span style="font-weight:900;">Telefono</span></label>
                  <input type='number' id='email' placeholder='Numero di telefono..' class='form-control' name="tel" required>

                  
                  <button class="btn btn-success" id="submit">Salva</button>
                </form>
              </figure>
          </div>
        </div>
      </div>
  </main>
</x-layout>