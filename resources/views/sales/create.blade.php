<x-layout>
    <main class="main-content">
        <div class="container" style="margin-top: 100px">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                @if (Session::has('danger'))
                    <div class="alert alert-danger">
                        {{session('danger')}}
                    </div>
                @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                </div>
            </div>
          </div>
        <form action="{{route('sales.store')}}" method="POST">
            @csrf
            <input type="text" name="waiter_id" value="{{$waiter_id}}" hidden>
            <input type="text" name="restaurant_id" value="{{Auth::user()->id_restaurant}}" hidden>
            <input type="text" name="user_id" value="{{Auth::user()->id}}" hidden>
            <div class="container-xxl">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <h2>Seleziona un vino</h2>
                        
                        <select name="wine_id" class="wine" id="">
                            <option value="0" selected>--</option>
                            @foreach ($wines as $w)
                            <option class="wineselected" name="wineselected" value="{{$w->id_wine}}-{{$w->qty}}-{{$w->qty_bicchieri}}">{{$w->wine->nome}} disponibilità: Bottiglie <span class="qty" value="{{$w->qty}}" style="color: red">{{$w->qty}}</span> Bicchieri <span class="qty" value="{{$w->qty_bicchieri}}" style="color: red">{{$w->qty_bicchieri}}</span></option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <h2>Seleziona vendita</h2>
                        
                        <select name="tipo" class="tipo" id="">
                            <option value="0" selected>--</option>
                            <option value="bottiglia">Bottiglia</option>
                            <option value="bicchiere">Bicchiere</option>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-6 text-center tavolo" hidden>
                        <label for="tavolo"><h2>numero del tavolo</h2></label><br>
                        <input type="number" name="tavolo" class="text-center form-control">
                    </div>
                    <div class="col-6 text-center quantita" hidden>
                        <label for="quantita"><h2>inserisci quantita</h2></label><br>
                        <input name="quantita" class="text-center qta form-control">
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-12 text-center">
                        <button type="submit" class="submit btn btn-dark" >Salva Vendita</button>
                    </div>
                </div>
            </div>
        </form>
    </main>

<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous">
</script>

<script>
    $('.wine').change(function (e) { 
        e.preventDefault();
        let wineselected=$(this).val();
        let split=wineselected.split('-');
        let wineid=split[0];
        let qty=parseInt(split[1]);
        let qty_bicchieri=parseInt(split[2]);
        /* console.log(qty_bicchieri); */
        
        
        
        if (wineselected!=0) {
            $('.tavolo').attr("hidden", false);
            $('.quantita').attr("hidden", false);
        }
        $('.tipo').change(function (e) { 
            e.preventDefault();
            let tipo=$(this).val();
        if (tipo=='bottiglia') {
            $('.qta').change(function (e) { 
            e.preventDefault();
            let qtysel=$(this).val();
            console.log(qtysel > qty);

            if (qtysel > qty) {
                alert("quantita selezionata maggiore della disponibilita")
                $('.qta').val(qty);
            }else{
                $('.submit').attr('disabled', false);
            }
            

        });
        }else{
            $('.qta').change(function (e) { 
            e.preventDefault();
            let qtysel=$(this).val();
            console.log(qtysel > qty_bicchieri);

            if (qtysel > qty_bicchieri) {
                alert("quantita selezionata maggiore della disponibilita")
                $('.qta').val(qty_bicchieri);
            }else{
                $('.submit').attr('disabled', false);
            }
            

        });
        }
        });
        
        
    });
    
    
</script>
 
</x-layout>