<x-layout>
    {{-- <?php dd($sales) ?> --}}
    <main class="main-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                @foreach ($waiter as $w)
                <h1>Vendite effettuate da {{$w->nome}}</h1>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container-xxl">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('sales.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;"> c r e a &nbsp; V e n d i t a</p></a>
            </div>
        </div>
    </div>
    <div class="container-xxl mt-5">
        <div class="row justify-content-center">
            @foreach ($sales as $sale)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
                <div class="card dropdown m-2">
                    <div class="content dropdown-content">
                    <div class="details p-4">
                    <div>
                        <h3>Data ordine {{$sale->date}}</h3>
                    </div>
                    <div>
                        <span>quantita Bottiglie: <span style="font-weight: 600;color:green">{{$sale->qty_bottiglie}}</span></span> <br>
                        <span>quantita Bicchieri: <span style="font-weight: 600;color:green">{{$sale->qty_bicchieri}}</span></span>
                    </div>
                    <div>
                        <span>data: <span style="font-weight: 600;color:green">{{$sale->created_at}}</span></span>
                    </div>
                    </div>
                </div>
                </div>
                </div>
            @endforeach
    </div>
    <div class="container-xxl mt-1">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            {{$sales->links()}}
          </div>
        </div>
    </div>
  </div>
</main>
</x-layout>