
<style>
@import "compass/css3";
@import url('https://fonts.googleapis.com/css2?family=Neonderthaw&display=swap');
/*  * {
	 box-sizing: border-box;
	 -moz-box-sizing: border-box;
	 -webkit-box-sizing: border-box;
	 font-family: arial;
} */

 h1 {
	 color: rgb(85, 8, 8);
	 text-align: center;
	 font-family: 'Neonderthaw', cursive;
	 font-size: 50px;
}
 .login-form {
	 width: 350px;
	 padding: 40px 30px;
	 background: #fefeffc9;
	 margin: auto;
	 position: absolute;
	 left: 0;
	 right: 0;
	 top: 30%;
     border-radius: 10px;
     
}
 .form-group {
	 position: relative;
	 margin-bottom: 15px;
     filter: blur(0px);
     
}
 .form-control {
	 width: 100%;
	 height: 50px;
	 border: none;
	 padding: 5px 7px 5px 15px;
	 background: #fff;
	 color: #666;
	 border: 2px solid #ddd;
     filter: blur(0px);

}
 .form-control:focus, .form-control:focus + .fa {
	 border-color: #10ce88;
	 color: #10ce88;
}
 .form-group .fa {
	 position: absolute;
	 right: 15px;
	 top: 17px;
	 color: #999;
}
 .log-status.wrong-entry .form-control, .wrong-entry .form-control + .fa {
	 border-color: #ed1c24;
	 color: #ed1c24;
}
 .log-btn {
	 background: #0ac986;
	 dispaly: inline-block;
	 width: 100%;
	 font-size: 16px;
	 height: 50px;
	 color: #fff;
	 text-decoration: none;
	 border: none;
}
 .link {
	 text-decoration: none;
	 color: #c6c6c6;
	 float: right;
	 font-size: 12px;
	 margin-bottom: 15px;
}
 .link:hover {
	 text-decoration: underline;
	 color: #8c918f;
}
 .alert {
	 display: none;
	 font-size: 12px;
	 color: #f00;
	 float: left;
}
.form-group .far {
    position: absolute;
    right: 15px;
    top: 17px;
    color: #999;
}
 


</style>
<x-layout>
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center" style="width: 50%">
                <form action="{{route('login')}}" method="POST">
                @csrf
                
                <div class="login-form shadow">
                    <h1 class="h1form">Enoteca Dell'Etna</h1>
                    <div class="form-group ">
                      <input type="text" class="form-control" placeholder="Username " id="UserName" name="email">
                      <i class="fa fa-user"></i>
                    </div>
                    <div class="form-group log-status">
                      <input type="password" class="form-control show" placeholder="Password" id="Passwod" name="password">
                      <input type="checkbox" class="check far fa-eye" style="appearance: none;">
                    </div>
                    <input class="errors" type="text" value="{{$errors}}" hidden>
                    @if (!$errors->isEmpty())
                    <span class="alert">Credenziali non valide</span>  
                    @else
                    <span class="alert">Credenziali valide</span>
                    @endif
                     
                     <a class="link" href="{{-- {{route('reset-password',['token' => $token])}} --}}">Password dimenticata?</a>
                    <button type="submit" class="log-btn" >Log in</button>
                  </div>
                </form>           
            </div>
        </div>
    </div>
    
<script>
      $(document).ready(function(){
          let errors=$('.errors').val();
        $('.log-btn').click(function(){
            $('.log-status').addClass('wrong-entry');
           $('.alert').fadeIn(500);
           setTimeout( "$('.alert').fadeOut(1500);",3000 );
        });
        $('.form-control').keypress(function(){
            $('.log-status').removeClass('wrong-entry');
        });
        $('.check').change(function (e) { 
            e.preventDefault();
            if ($(this).is(':checked')) {
                $('.show').attr('type', 'text');
                $('.check').removeClass('fa-eye');
                $('.check').addClass('fa-eye-slash');

            }else{
                $('.show').attr('type', 'password');
                $('.check').addClass('fa-eye');
                $('.check').removeClass('fa-eye-slash');
            }
             
            
            
        });

    });
</script>
</x-layout>