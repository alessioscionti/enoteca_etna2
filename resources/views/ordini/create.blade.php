<x-layout>
    <main class="main-content">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <h1>Stai creando un nuovo ordine</h1>
        </div>
    </div>
</div>
    @csrf
<div class="container-xxl mt-5">
    <div class="row">
        @foreach ($wines as $wine)
        <div class="col mt-5">
            <div class="card" style="width: 18rem;">
                <input class="wine-id" type="number" value="{{$wine->id}}" hidden>
                <input name="id-user" type="number" value="{{Auth::user()->id}}" hidden>
                <input class="id-wine" type="checkbox" name="wine[{{$wine->id}}]" id="wine" value="{{$wine->id}}" hidden>
                <img src="https://www.tascapan.com/wp-content/uploads/2014/07/Nus-Retro.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$wine->nome}}</h5><hr>
                  <label for="descrizione">Descrizione:</label>
                  <p class="card-text">{{$wine->descrizione}}</p>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">prezzo: {{$wine->prezzo_bottiglia}}€</li>

                </ul>
                <div class="row">
                    <div class="col-12">
                       <a href="{{route('ordini.winedetails',$wine)}}"><button type="submit" class="btn btn-success text-center m-3 p-2">Dettagli prodotto</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</div>
<div style="height: 200px"></div>

<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous"></script>
<script>
    $('input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $('#qty').attr("required",true);
        } else {
            $('#qty').attr("required",false);
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('.increment-btn').click(function (e) { 
            e.preventDefault();
            let inc_val=$('.quantity').val();
            let value= parseInt(inc_val,10);
            value= isNaN(value) ? 0: value;
            if (value<10) {
                value++;
                $('.quantity').val(value);
            }
        });
        $('.decrement-btn').click(function (e) { 
            e.preventDefault();
            let dec_val=$('.quantity').val();
            let value= parseInt(dec_val,10);
            value= isNaN(value) ? 0: value;
            if (value>1) {
                value--;
                $('.quantity').val(value);
            }
        });
    });
</script>
</main>
</x-layout>