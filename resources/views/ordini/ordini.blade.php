<style>
.card {
    position: relative;
    width: 500px;
    height: 300px;
    background: rgba(255, 255, 255, 0.1);
    backdrop-filter: blur(10px);
    -webkit-backdrop-filter: blur(10px);
    border-radius: 25px;
    box-shadow: 0 25px 25px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    border-top: 1px solid rgba(255, 255, 255, 0.25);
    transition: 1s;
    display: flex;
    justify-content: center;
    align-items: center;
}

.card .content {
    position: relative;
    display: flex;
    align-items: center;
    margin-top: -60px;
}

</style>

<x-layout>
<main class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('ordini.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;"> c r e a &nbsp; o r d i n e</p></a>
            </div>
        </div>
    </div>
    @if (Auth::user()->admin==1)
        <div class="container-xxl mt-5">
            <div class="row">
                @foreach ($orders as $order)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content mt-1">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                                    <div class="details">
                                        <div>
                                            <h3>ordine N° {{$order->id}}</h3>
                                        </div>
                                        <div>
                                            <span>ordinato il: <span style="font-weight: 600;color:green">{{$order->data_ordine}}</span></span>
                                        </div>
                                        <div>
                                            <span>costo ordine: <span style="font-weight: 600;color:green">{{$order->prezzo}}€</span></span>
                                        </div>
                                        <div>
                                            <span>Bottiglie Ordinate: <span style="font-weight: 600;color:green">{{$order->quantita}}</span></span>
                                        </div>
                                        <div>
                                            {{-- <ion-icon name="mail-outline"></ion-icon> --}}
                                            @if ($order->pagato==0||$order->pagato==2)
                                                <span>orine pagato:<span style="font-weight: 600;color:red"> NO </span></span>
                                            @else
                                                <span>orine pagato:<span style="font-weight: 600;color:green"> PAGATO </span>  </span>
                                            @endif 
                                        </div>
                                        <div>
                                            {{-- <ion-icon name="mail-outline"></ion-icon> --}}
                                            @if ($order->evaso==0)
                                                <span>orine evaso:<span style="font-weight: 600;color:red"> NO </span></span>
                                            @else
                                                <span>orine evaso:<span style="font-weight: 600;color:green"> EVASO </span> </span> 
                                            @endif 
                                        </div>
                                        <div>
                                            <span>Ristorante: <span style="font-weight: 600;color:green">{{$order->restaurant->ragione_sociale}}</span></span>
                                        </div>
                                        <a href="{{route('ordini.details',$order)}}" class="btn btn-warning">Modifica</a>
                                        @if ($order->pagato==0 || $order->pagato==2)
                                        <form action="{{ route('processTransaction') }}" method="POST">
                                            @csrf
                                        
                                            @if(\Session::has('error'))
                                                <div class="alert alert-danger">{{ \Session::get('error') }}</div>
                                                {{ \Session::forget('error') }}
                                            @endif
                                            @if(\Session::has('success'))
                                                <div class="alert alert-success">{{ \Session::get('success') }}</div>
                                                {{ \Session::forget('success') }}
                                            @endif
                                            <input type="text" name="order_id" value="{{$order->id}}" hidden>
                                            <label for="payorder">Paga con:</label><br>
                                            <button class="fab fa-cc-paypal" name="payorder" type="submit" style="border:none;font-size:2.5rem;color:#183153;"></button>
                                        </form> 
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="container-xxl mt-3 mb-1 me-1">
            <div class="row justify-content-center">
              <div class="col-12 text-center">
                {{$orders->links()}}
              </div>
            </div>
        </div>
      </div>  
    @else
    <div class="container-xxl mt-5">
        <div class="row">
            @foreach ($ordersrestaurant as $order)
            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                <div class="card m-3">
                    <div class="content mt-1">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                                <div class="details">
                                    <div>
                                            <h3>ordine N° {{$order->id}}</h3>
                                        </div>
                                        <div>
                                            <span>ordinato il: <span style="font-weight: 600;color:green">{{$order->data_ordine}}</span></span>
                                        </div>
                                        <div>
                                            <span>costo ordine: <span style="font-weight: 600;color:green">{{$order->prezzo}}€</span></span>
                                        </div>
                                        <div>
                                            <span>Bottiglie Ordinate: <span style="font-weight: 600;color:green">{{$order->quantita}}</span></span>
                                        </div>
                                        <div>
                                            {{-- <ion-icon name="mail-outline"></ion-icon> --}}
                                            @if ($order->pagato==0||$order->pagato==2)
                                            <span>orine pagato:<span style="font-weight: 600;color:red"> NO </span></span>
                                            @else
                                            <span>orine pagato:<span style="font-weight: 600;color:green"> PAGATO </span>  </span>
                                            @endif 
                                        </div>
                                        <div>
                                            {{-- <ion-icon name="mail-outline"></ion-icon> --}}
                                            @if ($order->evaso==0)
                                            <span>orine evaso:<span style="font-weight: 600;color:red"> NO </span></span>
                                            @else
                                            <span>orine evaso:<span style="font-weight: 600;color:green"> EVASO </span> </span> 
                                            @endif 
                                        </div>
                                        <div>
                                            <span>Ristorante: <span style="font-weight: 600;color:green">{{$order->restaurant->ragione_sociale}}</span></span>
                                        </div>
                                        <a href="{{route('ordini.details',$order)}}" class="btn btn-warning">Modifica</a>
                                        @if ($order->pagato==0 || $order->pagato==2)
                                        <form action="{{ route('processTransaction') }}" method="POST">
                                            @csrf
                                        
                                            @if(\Session::has('error'))
                                                <div class="alert alert-danger">{{ \Session::get('error') }}</div>
                                                {{ \Session::forget('error') }}
                                            @endif
                                            @if(\Session::has('success'))
                                                <div class="alert alert-success">{{ \Session::get('success') }}</div>
                                                {{ \Session::forget('success') }}
                                            @endif
                                            <input type="text" name="order_id" value="{{$order->id}}" hidden>
                                            <label for="payorder">Paga con:</label><br>
                                            <button class="fab fa-cc-paypal my-1" name="payorder" type="submit" style="border:none;font-size:2.5rem;color:#183153;"></button>
                                        </form> 
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container-xxl mt-1">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            {{$ordersrestaurant->links()}}
            </div>
        </div>
    </div>
    @endif
</main>
</x-layout>