<x-layout>
<main class="main-content">
<div class="container-xxl">
    <div class="row justify-content-center">
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img src="https://www.tascapan.com/wp-content/uploads/2014/07/Nus-Retro.jpg" class="card-img-top" alt="..." style="min-width: 80%; max-width:80%">
        </div>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 product">
            <input type="text" name="{{$wine->id}}" class="product-id" value="{{$wine->id}}" hidden>
            <input type="text" name="{{Auth::user()->id}}" class="user-id" value="{{Auth::user()->id}}" hidden>
            <h1>{{$wine->nome}}</h1>
            <p class="mt-3">{{$wine->descrizione}}</p>
            <br>
            <h5>Prezzo <span style="font-weight: 900;color:green">{{$wine->prezzo_bottiglia}}€</span></h5>
            <br>
            <h5>Prezzo a Blocco <span style="font-weight: 900;color:green">{{$wine->prezzo_blocco}}€</span></h5>
            <br>
            <h5>Disponibilita: <span style="font-weight: 900;color:green">{{$wine->quantita}}</span></h5>
            <br>
            <label for="quantity">quantità richiesta:</label><br>
            <button type="button" class="btn btn-danger decrement-btn" style="width: 27px;height:27px;margin-right:-1px;margin-top:-1px"><p>-</p></button>
            <input type="number" name="quantity[{{$wine->id}}]" class="quantity rounded text-center" value="" id="qty" style="width: 10%;border:1px solid grey">
            <button type="button" class="btn btn-success increment-btn" style="width: 27px;height:27px;margin-left:-1px;margin-top:-1px">+</button><br><br>
            <button class="btn btn-warning add-cart">Aggiungi al carrello</button>
        </div>
    </div>
</div>
</div><hr style="border:2px solid #11101d;opacity:1;border-radius:100%;">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <a class="fas fa-arrow-circle-left" href="{{route('ordini.create')}}" style="font-size: 3rem;text-decoration:none;color:#11101d"></a>
        </div>
    </div>
</div>

{{-- SCRIPT --}}
<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.add-cart').click(function (e) { 
            e.preventDefault();
            let product_id=$(this).closest('.product').find('.product-id').val();
            let user_id=$(this).closest('.product').find('.user-id').val();
            let quantity=$(this).closest('.product').find('.quantity').val();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "/add-cart",
                data: {
                    'product_id':product_id,
                    'quantity':quantity,
                    'user_id':user_id,
                },
                success: function (response) {
                    /* alert(JSON.stringify(response)); */
                    alert(response);
                    /* alert(wine);
                    alert(qty); */
                    $("#numbercart").load(location.href + " #numbercart");
                }
            });
            
        });

        $('.increment-btn').click(function (e) { 
            e.preventDefault();
            let inc_val=$('.quantity').val();
            let value= parseInt(inc_val,10);
            value= isNaN(value) ? 0: value;
            if (value<10) {
                value++;
                $('.quantity').val(value);
            }
        });
        $('.decrement-btn').click(function (e) { 
            e.preventDefault();
            let dec_val=$('.quantity').val();
            let value= parseInt(dec_val,10);
            value= isNaN(value) ? 0: value;
            if (value>1) {
                value--;
                $('.quantity').val(value);
            }
        });
    });
</script>
</main>
</x-layout>