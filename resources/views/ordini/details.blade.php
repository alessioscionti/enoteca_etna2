<x-layout>
<main class="main-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>MODIFICA ORDINE N° {{$order->id}}</h1>
            </div>
        </div>
    </div><hr style="border:2px solid #11101d;opacity:1;border-radius:100%;">
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>RIEPILOGO ORDINE</h1>
            </div>
        </div>
        <form action="{{route('pdf.bolla', $order)}}" method="POST">
        @csrf
        <div class="row justify-content-center mt-5">
            <div class="col-12">
                <h3>Ordine effettuato da <span style="color:green">{{$order->restaurant->ragione_sociale}}</span></h3>
                <h3>In Data: <span style="color:green">{{$order->data_ordine}}</span></h3>
            </div>
        </div>
        <div class="row justify-content-center mt-5 align-items-center">
            <div class="col-12 col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
                <h3>Bottiglie ordinate:</h3>
            </div>
            <div class="col-12 col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
                <div class="container">
                    <div class="row justify-content-center">
                        @foreach ($winecount as $item)
                        
                        <div class="col pt-5 pb-5 mt-3" style="border:1px solid #11101d;opacity:1;border-radius:2%;margin-right:1rem;box-shadow: 0 8px 6px -6px rgb(99, 1, 1);min-width:200px;max-width:50%">
                            <h4 style="color: green">{{$item->qty}}x <span style="color: black">{{$item->wine->nome}}</span></h4>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <input type="text" name="idorder" value="{{$order->id}}" hidden>
        <input type="text" name="ragione_sociale" value="{{$order->restaurant->ragione_sociale}}" hidden>
        <input type="text" name="data_ordine" value="{{$order->data_ordine}}" hidden>
        <input type="text" name="costo_ordine" value="{{$order->prezzo}}" hidden>
        <button type="submit" class="btn btn-dark">Crea PDF</button>
        </form>
        <div class="row justify-content-center mt-5">
            <div class="col">
                @if ($order->pagato==1)
                <h4>Costo Ordine: <span style="color:green">{{$order->prezzo}} €</span></h4>
                @else
                <h4>Costo Ordine: <span style="color:red">{{$order->prezzo}} €</span></h4>
                @endif
            </div>
        </div>
        <form action="{{route('ordini.update',$order)}}" method="POST">
            @csrf
            <div class="row justify-content-center mt-5">
                <div class="col">
                    @if ($order->pagato==1)
                    <h4>Pagamento: <span style="color:green"> PAGATO</span></h4>
                    @if (Auth::user()->admin==1)
                    <button name="pagato" class="btn btn-danger" type="submit" value="no">IMPOSTA COME NON PAGATO</button>
                    @endif
                    @else
                    <h4>Pagamento:<span style="color:red"> NON PAGATO<span></h4>
                    @if (Auth::user()->admin==1)
                    <button name="pagato" class="btn btn-success" type="submit" value="si">IMPOSTA COME PAGATO</button>
                    @endif
                    @endif
                </div>
            </div>
        </form>
        <form action="{{route('ordini.update',$order)}}" method="POST">
            @csrf
            <div class="row justify-content-center mt-5">
                <div class="col">
                    @if ($order->evaso==1)
                    <h4>Evaso: <span style="color:green"> EVASO</span></h4>
                    @if (Auth::user()->admin==1)
                    <button name="evaso" class="btn btn-danger" type="submit" value="no">IMPOSTA COME NON EVASO</button>
                    @endif
                    @else
                    <h4>Evaso:<span style="color:red"> NON EVASO<span></h4>
                    @if (Auth::user()->admin==1)
                    <button name="evaso" class="btn btn-success" type="submit" value="si">IMPOSTA COME EVASO</button>
                    @endif
                    @endif
                </div>
            </div>
        </form>

    </div><hr style="border:2px solid #11101d;opacity:1;border-radius:100%;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a class="fas fa-arrow-circle-left" href="{{route('ordini.ordini')}}" style="font-size: 3rem;text-decoration:none;color:#11101d"></a>
            </div>
        </div>
    </div>
</main>
</x-layout>