<style>
.card {
    transition: 1s;

}

</style>

<x-layout>
    <main class="main-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <a href="{{route('wines.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;"> i n s e r i s c i &nbsp; v i n i</p></a>
                </div>
            </div>
        </div>
        <form action="{{route('filter.wine')}}" method="GET">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                        <label for="brand">Casa Vinicola</label><br>
                        <select name="brand" class="filter" id="">
                            <option value="" selected>--</option>
                            @foreach ($wines as $itembrand)
                            <option value="{{$itembrand->casa_vinicola}}">{{$itembrand->casa_vinicola}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                        <label for="nome">Nome</label><br>
                        <input type="text" name="nome" placeholder="cerca un vino" class="form-control" style="height: 1.5em">
                    </div>
                    <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                        <label for="prezzo">Prezzo</label><br>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-6">
                                    <input type="text" name="min" placeholder="minimo" class="form-control" style="height: 1.5em">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="max" placeholder="massimo" class="form-control" style="height: 1.5em">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xs-12 col-sx-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3 text-center">
                        <label for="categorie">Categorie</label><br>
                        <select name="categorie" class="filter" id="">
                            <option value="" selected>--</option>
                            @foreach ($wines as $itemcat)
                            <option value="{{$itemcat->id_categoria}}">{{$itemcat->categoria->nome_categoria}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-dark mt-3">Applica</button>
                </div>
                </div>
            </div>
        </form>
        @if (!$filterbrand->isEmpty())
        <div class="container-xxl mt-3">
            <div class="row align-bottom">
                @foreach ($filterbrand as $winebrand)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content my-5 me-3">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                    <h3>{{$winebrand->nome}}</h3>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                    <div class="avatar d-flex justify-content-center">
                                    <a href="{{route('wines.details',$winebrand)}}"><img src="{{asset('storage/img/vino.png')}}" width="20" height="20" alt="avatar"></a>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                    <div class="details" style="margin-top: -2rem!important">
                                        <div>
                                            <H4>ID: {{$winebrand->id}}</H4>
                                            @if ($winebrand->id_categoria==1)
                                            <H4>Categoria: <span style="color: rgb(168, 3, 3)">{{$winebrand->categoria->nome_categoria}}</span></H4>
                                            @elseif($winebrand->id_categoria==2)
                                            <H4>Categoria: <span style="color:pink">{{$winebrand->categoria->nome_categoria}}</span></H4>
                                            @elseif($winebrand->id_categoria==3)
                                            <H4>Categoria: <span style="color:rgb(243, 156, 49)">{{$winebrand->categoria->nome_categoria}}</span></H4>
                                            @elseif($winebrand->id_categoria==4)
                                            <H4>Categoria: <span style="color:black ">{{$winebrand->categoria->nome_categoria}}</span></H4>
                                            @elseif($winebrand->id_categoria==5)
                                            <H4>Categoria: <span style="color:black">{{$winebrand->categoria->nome_categoria}}</span></H4>
                                            @endif
                                            <H5>Brand: {{$winebrand->casa_vinicola}}</H5>
                                        </div>
                                        <div>
                                            
                                            <span><span style="font-weight: 900">Descrizione:</span> <br>{{$winebrand->descrizione}}</span>
                                        </div>
                                        <div>
                                            <span style="font-size: 1.3rem; color:black;font-weight:900">Quantita: <span style="font-size: 1.2rem; color:green;font-weight:900">{{$winebrand->quantita}}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @elseif(!$filtercat->isEmpty())
        <div class="container-xxl mt-3">
            <div class="row align-bottom">
                @foreach ($filtercat as $winecat)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content my-5 me-3">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                    <h3>{{$winecat->nome}}</h3>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                    <div class="avatar d-flex justify-content-center">
                                    <a href="{{route('wines.details',$winecat)}}"><img src="{{asset('storage/img/vino.png')}}" width="20" height="20" alt="avatar"></a>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                    <div class="details" style="margin-top: -2rem!important">
                                        <div>
                                            <H4>ID: {{$winecat->id}}</H4>
                                            @if ($winecat->id_categoria==1)
                                            <H4>Categoria: <span style="color: rgb(168, 3, 3)">{{$winecat->categoria->nome_categoria}}</span></H4>
                                            @elseif($winecat->id_categoria==2)
                                            <H4>Categoria: <span style="color:pink">{{$winecat->categoria->nome_categoria}}</span></H4>
                                            @elseif($winecat->id_categoria==3)
                                            <H4>Categoria: <span style="color:rgb(243, 156, 49)">{{$winecat->categoria->nome_categoria}}</span></H4>
                                            @elseif($winecat->id_categoria==4)
                                            <H4>Categoria: <span style="color:black ">{{$winecat->categoria->nome_categoria}}</span></H4>
                                            @elseif($winecat->id_categoria==5)
                                            <H4>Categoria: <span style="color:black">{{$winecat->categoria->nome_categoria}}</span></H4>
                                            @endif
                                            <H5>Brand: {{$winecat->casa_vinicola}}</H5>
                                        </div>
                                        <div>
                                            
                                            <span><span style="font-weight: 900">Descrizione:</span> <br>{{$winecat->descrizione}}</span>
                                        </div>
                                        <div>
                                            <span style="font-size: 1.3rem; color:black;font-weight:900">Quantita: <span style="font-size: 1.2rem; color:green;font-weight:900">{{$winecat->quantita}}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @elseif(!$filterprice->isEmpty())
        <div class="container-xxl mt-3">
            <div class="row align-bottom">
                @foreach ($filterprice as $wineprice)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content my-5 me-3">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                    <h3>{{$wineprice->nome}}</h3>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                    <div class="avatar d-flex justify-content-center">
                                    <a href="{{route('wines.details',$wineprice)}}"><img src="{{asset('storage/img/vino.png')}}" width="20" height="20" alt="avatar"></a>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                    <div class="details" style="margin-top: -2rem!important">
                                        <div>
                                            <H4>ID: {{$wineprice->id}}</H4>
                                            @if ($wineprice->id_categoria==1)
                                            <H4>Categoria: <span style="color: rgb(168, 3, 3)">{{$wineprice->categoria->nome_categoria}}</span></H4>
                                            @elseif($wineprice->id_categoria==2)
                                            <H4>Categoria: <span style="color:pink">{{$wineprice->categoria->nome_categoria}}</span></H4>
                                            @elseif($wineprice->id_categoria==3)
                                            <H4>Categoria: <span style="color:rgb(243, 156, 49)">{{$wineprice->categoria->nome_categoria}}</span></H4>
                                            @elseif($wineprice->id_categoria==4)
                                            <H4>Categoria: <span style="color:black ">{{$wineprice->categoria->nome_categoria}}</span></H4>
                                            @elseif($wineprice->id_categoria==5)
                                            <H4>Categoria: <span style="color:black">{{$wineprice->categoria->nome_categoria}}</span></H4>
                                            @endif
                                            <H5>Brand: {{$wineprice->casa_vinicola}}</H5>
                                        </div>
                                        <div>
                                            
                                            <span><span style="font-weight: 900">Descrizione:</span> <br>{{$wineprice->descrizione}}</span>
                                        </div>
                                        <div>
                                            <span style="font-size: 1.3rem; color:black;font-weight:900">Quantita: <span style="font-size: 1.2rem; color:green;font-weight:900">{{$wineprice->quantita}}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @elseif(!$filtername->isEmpty())
        <div class="container-xxl mt-3">
            <div class="row align-bottom">
                @foreach ($filtername as $wine)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content my-5 me-3">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                    <h3>{{$wine->nome}}</h3>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                    <div class="avatar d-flex justify-content-center">
                                    <a href="{{route('wines.details',$wine)}}"><img src="{{asset('storage/img/vino.png')}}" width="20" height="20" alt="avatar"></a>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                    <div class="details" style="margin-top: -2rem!important">
                                        <div>
                                            <H4>ID: {{$wine->id}}</H4>
                                            @if ($wine->id_categoria==1)
                                            <H4>Categoria: <span style="color: rgb(168, 3, 3)">{{$wine->categoria->nome_categoria}}</span></H4>
                                            @elseif($wine->id_categoria==2)
                                            <H4>Categoria: <span style="color:pink">{{$wine->categoria->nome_categoria}}</span></H4>
                                            @elseif($wine->id_categoria==3)
                                            <H4>Categoria: <span style="color:rgb(243, 156, 49)">{{$wine->categoria->nome_categoria}}</span></H4>
                                            @elseif($wine->id_categoria==4)
                                            <H4>Categoria: <span style="color:black ">{{$wine->categoria->nome_categoria}}</span></H4>
                                            @elseif($wine->id_categoria==5)
                                            <H4>Categoria: <span style="color:black">{{$wine->categoria->nome_categoria}}</span></H4>
                                            @endif
                                            <H5>Brand: {{$wine->casa_vinicola}}</H5>
                                        </div>
                                        <div>
                                            
                                            <span><span style="font-weight: 900">Descrizione:</span> <br>{{$wine->descrizione}}</span>
                                        </div>
                                        <div>
                                            <span style="font-size: 1.3rem; color:black;font-weight:900">Quantita: <span style="font-size: 1.2rem; color:green;font-weight:900">{{$wine->quantita}}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>



        @else
        <div class="container-xxl mt-3">
            <div class="row align-bottom">
                @foreach ($wines as $winenofilter)
                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-flex justify-content-center align-content-center">
                    <div class="card m-3">
                        <div class="content my-5 me-3">
                            <div class="row">
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 text-center pt-3">
                                    <h3>{{$winenofilter->nome}}</h3>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                                    <div class="avatar d-flex justify-content-center">
                                    <a href="{{route('wines.details',$winenofilter)}}"><img src="{{asset("storage/vini/".$winenofilter->id."/".$winenofilter->id.".jpg")}}" width="20" height="20" alt="avatar"></a>
                                    </div>
                                </div>
                                <div class="col-12 col-xs-12 col-sx-12 col-md-12 col-lg-7 col-xl-7 col-xxl-7 mt-5">
                                    <div class="details" style="margin-top: -2rem!important">
                                        <div>
                                            <H4>ID: {{$winenofilter->id}}</H4>
                                            @if ($winenofilter->id_categoria==1)
                                            <H4>Categoria: <span style="color: rgb(168, 3, 3)">{{$winenofilter->categoria->nome_categoria}}</span></H4>
                                            @elseif($winenofilter->id_categoria==2)
                                            <H4>Categoria: <span style="color:pink">{{$winenofilter->categoria->nome_categoria}}</span></H4>
                                            @elseif($winenofilter->id_categoria==3)
                                            <H4>Categoria: <span style="color:rgb(243, 156, 49)">{{$winenofilter->categoria->nome_categoria}}</span></H4>
                                            @elseif($winenofilter->id_categoria==4)
                                            <H4>Categoria: <span style="color:black ">{{$winenofilter->categoria->nome_categoria}}</span></H4>
                                            @elseif($winenofilter->id_categoria==5)
                                            <H4>Categoria: <span style="color:black">{{$winenofilter->categoria->nome_categoria}}</span></H4>
                                            @endif
                                            <H5>Brand: {{$winenofilter->casa_vinicola}}</H5>
                                        </div>
                                        <div>
                                            
                                            <span><span style="font-weight: 900">Descrizione:</span> <br>{{$winenofilter->descrizione}}</span>
                                        </div>
                                        <div>
                                            <span style="font-size: 1.3rem; color:black;font-weight:900">Quantita: <span style="font-size: 1.2rem; color:green;font-weight:900">{{$winenofilter->quantita}}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>


        @endif

    </main>
</x-layout>