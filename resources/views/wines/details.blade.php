<x-layout>
<main class="main-content">
<div class="container-xxl">
    <div class="row justify-content-center">
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <img src="{{asset('storage/img/vino.png')}}" class="card-img-top" alt="..." style="min-width: 80%; max-width:80%">
        </div>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
            <h1>{{$wine->nome}}</h1>
            <p class="mt-3">{{$wine->descrizione}}</p>
            <br>
            <h5>Categoria <span style="font-weight: 900;color:green">{{$wine->categoria->nome_categoria}}</span></h5>
            <br>
            <h5>Prezzo <span style="font-weight: 900;color:green">{{$wine->prezzo_bottiglia}}€</span></h5>
            <br>
            <h5>Prezzo a Bicchiere <span style="font-weight: 900;color:green">{{$wine->prezzo_bicchiere}}€</span></h5>
            <br>
            <h5>Quantita Bicchieri vendibili <span style="font-weight: 900;color:green">{{$wine->quantita_bicchieri}}</span></h5>
            <br>
            <h5>Disponibilita: <span style="font-weight: 900;color:green">{{$wine->quantita}}</span></h5>
        </div>
    </div>
</div>
</main>
</x-layout>