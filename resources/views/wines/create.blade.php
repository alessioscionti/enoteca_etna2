<style>
    .login-form {

    background: #fefeffc9;
    margin: auto;
    position: absolute;
    left: 0;
    right: 0;
    top: 20%!important;
    border-radius: 10px;
}
main {
    margin-top: 85px;
    padding: 2rem 1.5rem;
    background: linear-gradient(rgba(255, 255, 255, .8), rgba(255, 255, 255, .8)), url(https://vininativ.it/wp-content/uploads/2020/04/2768481.jpg) 50%/ cover #eee;
    background-blend-mode: luminosity;
    height: 160vh;
}
</style>
<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                    <h1 id='title'>Inserisci Nuovo Vino</h1>
                    <figure class="login-form shadow">
                    <form id='survey-form' method="POST" action="{{route('wines.store')}}"enctype="multipart/form-data">
                        @csrf
                        <label id='name-label'>Nome Vino</label>
                        <input type='text' id='name' name="nome" placeholder='Nome..' class='form-control' required>
                        <hr>
                        <label id='name-label'>Descrizione</label>
                        <input type='text' id='name' name="descrizione" placeholder='descrizione..' class='form-control' required>
                        <hr>
                        <label id='name-label'>Casa Vinicola</label>
                        <input type='text' id='name' name="brand" placeholder='Casa Vinicola..' class='form-control' required>
                        <hr>
                        <label id='number-label'>Prezzo a Bottiglia</label>
                        <input type='number' id='number' placeholder="€.." class='form-control' name="prezzo_bottiglia" required>
                        <hr>
                        <label id='number-label'>Costo a Bottiglia</label>
                        <input type='number' id='number' placeholder="€.." class='form-control' name="costo_bottiglia" required>
                        <hr>
                        <label id='number-label'>Prezzo a Bicchiere</label>
                        <input type='number' id='number' placeholder="€.." class='form-control' name="prezzo_bicchiere" required>
                        <hr>
                        <label id='number-label'>Costo a Bicchiere</label>
                        <input type='number' id='number' placeholder="€.." class='form-control' name="costo_bicchiere" required>
                        <hr>
                        <label id='email-label'>Quantita Bicchieri</label>
                        <input type='number' id='email' class='form-control' name="quantita_bicchieri" required>
                        <hr>
                        <label id='email-label'>Quantita</label>
                        <input type='number' id='email' class='form-control' name="quantita" required>
                        <hr>
                        <label id='email-label'>Categoria</label>
                        <select name="categoria" id="">
                            @foreach ($category as $cat)
                                <option value="{{$cat->id}}">{{$cat->nome_categoria}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <label id='email-label'>Immagine</label>
                        <input type='file' id='email' class='form-control' name="immagine" required>
                        <hr>

                        <button class="btn btn-success" id="submit">Salva</button>
                    </form>
                </figure>
            </div>
        </div>
    </div>
</main>
</x-layout>