<?php

namespace Database\Seeders;

use App\Models\Restaurant;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $restaurants = [
            [
              "ragione_sociale" => "Enoteca",
              "indirizzo" => "via dittaino 15",
              "citta" => "catania",
              "provincia" => "CT" ,
              "cap" => "95121",
              'partita_iva'=>"0123456789",
              'pec'=>"test@pec.it",
              'codice_univoco'=>"COD01234",
              'telefono'=>"0123456789",
              
            ],
            [
              "ragione_sociale" => "Enoteca 2",
              "indirizzo" => "via dittaino 15",
              "citta" => "catania",
              "provincia" => "CT" ,
              "cap" => "95121",
              'partita_iva'=>"0123456789",
              'pec'=>"test@pec.it",
              'codice_univoco'=>"COD01234",
              'telefono'=>"0123456789",
              
            ],
            [
              "ragione_sociale" => "Enoteca 3",
              "indirizzo" => "via dittaino 15",
              "citta" => "catania",
              "provincia" => "CT" ,
              "cap" => "95121",
              'partita_iva'=>"0123456789",
              'pec'=>"test@pec.it",
              'codice_univoco'=>"COD01234",
              'telefono'=>"0123456789",
              
            ]];
            foreach($restaurants as $restaurant) {

                $newRestaurant = new Restaurant;
                $newRestaurant->ragione_sociale = $restaurant["ragione_sociale"];
                $newRestaurant->indirizzo = $restaurant["indirizzo"];
                $newRestaurant->citta = $restaurant["citta"];
                $newRestaurant->provincia = $restaurant["provincia"];
                $newRestaurant->cap = $restaurant["cap"];
                $newRestaurant->partita_iva = $restaurant["partita_iva"];
                $newRestaurant->pec = $restaurant["pec"];
                $newRestaurant->codice_univoco = $restaurant["codice_univoco"];
                $newRestaurant->telefono = $restaurant["telefono"];
                $newRestaurant->save();
              }
    }
}
