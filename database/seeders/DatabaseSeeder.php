<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\WineSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\RestaurantSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RestaurantSeeder::class,
            UserSeeder::class,
            WaiterSeeder::class,
            CategorySeeder::class,
            WineSeeder::class,
        ]);
    }
}
