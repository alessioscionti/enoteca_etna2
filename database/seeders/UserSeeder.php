<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
              "name" => "Alessio",
              // "date_of_birth" => "1994-02-17",
              "email" => "alessioscionti@gmail.com",
              "admin" => "1" ,
              "moderator" => "0",
              'waiters'=>"0",
              'id_restaurant'=>"1",
            ],
            [
              "name" => "ristorante 2",
              // "date_of_birth" => "1994-02-17",
              "email" => "ristorante2@gmail.com",
              "admin" => "0" ,
              "moderator" => "1",
              'waiters'=>"0",
              'id_restaurant'=>"2",
            ],
            [
              "name" => "ristorante 3",
              // "date_of_birth" => "1994-02-17",
              "email" => "ristorante3@gmail.com",
              "admin" => "0" ,
              "moderator" => "1",
              'waiters'=>"0",
              'id_restaurant'=>"3",
            ],
            [
              "name" => "cameriere 1",
              // "date_of_birth" => "1994-02-17",
              "email" => "cameriere@gmail.com",
              "admin" => "0" ,
              "moderator" => "0",
              'waiters'=>"1",
              'id_restaurant'=>"1",
            ],
            [
              "name" => "cameriere 2",
              // "date_of_birth" => "1994-02-17",
              "email" => "cameriere2@gmail.com",
              "admin" => "0" ,
              "moderator" => "0",
              'waiters'=>"1",
              'id_restaurant'=>"2",
            ],
            [
              "name" => "cameriere 3",
              // "date_of_birth" => "1994-02-17",
              "email" => "cameriere3@gmail.com",
              "admin" => "0" ,
              "moderator" => "0",
              'waiters'=>"1",
              'id_restaurant'=>"3",
            ]];
            foreach($users as $user) {

                $newUser = new User;
                $newUser->name = $user["name"];
                $newUser->email = $user["email"];
                $newUser->password = Hash::make('Alessio123');
                $newUser->admin = $user["admin"];
                $newUser->moderator = $user["moderator"];
                $newUser->waiters = $user["waiters"];
                $newUser->id_restaurant = $user["id_restaurant"];
                $newUser->save();
              }
    }
}
