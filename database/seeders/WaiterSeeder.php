<?php

namespace Database\Seeders;

use App\Models\Waiter;
use Illuminate\Database\Seeder;

class WaiterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $waiters=[[
            'user_id'=>'4',
            'restaurant_id'=>'1',
            'nome'=>'cameriere 1',
            'cognome'=>'scionti',
            'punti'=>'0',
            'provvigioni'=>'0',
        ],
        [
            'user_id'=>'5',
            'restaurant_id'=>'2',
            'nome'=>'cameriere 2',
            'cognome'=>'scionti',
            'punti'=>'0',
            'provvigioni'=>'0',
        ],
        [
            'user_id'=>'6',
            'restaurant_id'=>'3',
            'nome'=>'cameriere 3',
            'cognome'=>'scionti',
            'punti'=>'0',
            'provvigioni'=>'0',
        ],
        
    ];
        foreach ($waiters as $waiter) {
            $newWaiter=new Waiter;
            $newWaiter->user_id=$waiter['user_id'];
            $newWaiter->restaurant_id=$waiter['restaurant_id'];
            $newWaiter->nome=$waiter['nome'];
            $newWaiter->cognome=$waiter['cognome'];
            $newWaiter->punti=$waiter['punti'];
            $newWaiter->provvigioni=$waiter['provvigioni'];
            $newWaiter->save();
        }
    }
}
