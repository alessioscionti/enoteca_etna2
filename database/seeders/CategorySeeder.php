<?php

namespace Database\Seeders;

use App\Models\Categorie;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            [
              "nome_categoria" => "Rosso",
            ],
            [
                "nome_categoria" => "Rosato",
            ],
            [
                "nome_categoria" => "Bianco",
            ],
            [
                "nome_categoria" => "Bollicine",
            ],
            [
                "nome_categoria" => "Liquori",
            ]];

            foreach($category as $categoria) {

                $newcategoria = new Categorie();
                $newcategoria->nome_categoria = $categoria["nome_categoria"];
                $newcategoria->save();
              }
    }
}
