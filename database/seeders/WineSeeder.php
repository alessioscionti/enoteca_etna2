<?php

namespace Database\Seeders;

use App\Models\Wine;
use Illuminate\Database\Seeder;

class WineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wines=[
        [
            'nome'=>'vino 1',
            'descrizione'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, impedit explicabo adipisci vitae rerum perspiciatis dolor dicta in pariatur atque tempora, suscipit quas. Incidunt exercitationem iusto ab eveniet, dolorum optio.',
            'casa_vinicola'=>'Antico Vinaio',
            'prezzo_bottiglia'=>"10.00",
            'costo_bottiglia'=>"10.00",
            'prezzo_bicchiere'=>"10.00",
            'costo_bicchiere'=>"10.00",
            'quantita_bicchieri'=>"10",
            'quantita'=>"1000",
            'id_categoria'=>"1",
        ],
        [
            'nome'=>'vino 2',
            'descrizione'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, impedit explicabo adipisci vitae rerum perspiciatis dolor dicta in pariatur atque tempora, suscipit quas. Incidunt exercitationem iusto ab eveniet, dolorum optio.',
            'casa_vinicola'=>'Antico Vinaio',
            'prezzo_bottiglia'=>"10.00",
            'costo_bottiglia'=>"10.00",
            'prezzo_bicchiere'=>"10.00",
            'costo_bicchiere'=>"10.00",
            'quantita_bicchieri'=>"10",
            'quantita'=>"1000",
            'id_categoria'=>"2",  
        ],
        [
            'nome'=>'vino 3',
            'descrizione'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, impedit explicabo adipisci vitae rerum perspiciatis dolor dicta in pariatur atque tempora, suscipit quas. Incidunt exercitationem iusto ab eveniet, dolorum optio.',
            'casa_vinicola'=>'Antico Vinaio',
            'prezzo_bottiglia'=>"10.00",
            'costo_bottiglia'=>"10.00",
            'prezzo_bicchiere'=>"10.00",
            'costo_bicchiere'=>"10.00",
            'quantita_bicchieri'=>"10",
            'quantita'=>"1000",
            'id_categoria'=>"3",
        ],
        [
            'nome'=>'vino 4',
            'descrizione'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, impedit explicabo adipisci vitae rerum perspiciatis dolor dicta in pariatur atque tempora, suscipit quas. Incidunt exercitationem iusto ab eveniet, dolorum optio.',
            'casa_vinicola'=>'Antico Vinaio',
            'prezzo_bottiglia'=>"10.00",
            'costo_bottiglia'=>"10.00",
            'prezzo_bicchiere'=>"10.00",
            'costo_bicchiere'=>"10.00",
            'quantita_bicchieri'=>"10",
            'quantita'=>"1000",
            'id_categoria'=>"4",
        ],
        [
            'nome'=>'vino 5',
            'descrizione'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, impedit explicabo adipisci vitae rerum perspiciatis dolor dicta in pariatur atque tempora, suscipit quas. Incidunt exercitationem iusto ab eveniet, dolorum optio.',
            'casa_vinicola'=>'Antico Vinaio',
            'prezzo_bottiglia'=>"10.00",
            'costo_bottiglia'=>"10.00",
            'prezzo_bicchiere'=>"10.00",
            'costo_bicchiere'=>"10.00",
            'quantita_bicchieri'=>"10",
            'quantita'=>"1000",
            'id_categoria'=>"5",
        ],

    ];
        foreach ($wines as $wine ) {
            $newWine= new Wine;
            $newWine->nome=$wine['nome'];
            $newWine->descrizione=$wine['descrizione'];
            $newWine->casa_vinicola=$wine['casa_vinicola'];
            $newWine->prezzo_bottiglia=$wine['prezzo_bottiglia'];
            $newWine->costo_bottiglia=$wine['costo_bottiglia'];
            $newWine->prezzo_bicchiere=$wine['prezzo_bicchiere'];
            $newWine->costo_bicchiere=$wine['costo_bicchiere'];
            $newWine->quantita=$wine['quantita'];
            $newWine->quantita_bicchieri=$wine['quantita_bicchieri'];
            $newWine->id_categoria=$wine['id_categoria'];
            $newWine->save();
        }
    }
}
