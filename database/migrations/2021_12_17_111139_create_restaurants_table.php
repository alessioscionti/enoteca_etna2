<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->text('ragione_sociale');
            $table->text('indirizzo');
            $table->text('citta');
            $table->text('provincia');
            $table->text('pec')->nullable();
            $table->bigInteger('cap');
            $table->bigInteger('partita_iva');
            $table->text('codice_univoco')->nullable();
            $table->bigInteger('telefono')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}