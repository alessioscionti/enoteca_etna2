<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wines', function (Blueprint $table) {
            $table->id();
            $table->text('nome');
            $table->text('descrizione');
            $table->text('casa_vinicola');
            $table->decimal('prezzo_bottiglia',10,2);
            $table->decimal('costo_bottiglia',10,2);
            $table->decimal('prezzo_bicchiere',10,2);
            $table->decimal('costo_Bicchiere',10,2);
            $table->bigInteger('quantita');
            $table->bigInteger('quantita_bicchieri');
            $table->unsignedBigInteger('id_categoria');
            $table->foreign('id_categoria')->references('id')->on('categories');
            $table->text('immagine')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wines');
    }
}
